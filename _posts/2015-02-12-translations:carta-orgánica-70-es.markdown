---
title: Translations:Carta Orgánica/70/es
layout: post
categories: []
---

La Asamblea Permanente convocará a Asamblea General con una antelación
definida por reglamento no menor a los quince (15) días y mediante
comunicación fehaciente de la Orden del Día a todos los Piratas
Afiliados.
