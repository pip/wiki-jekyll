---
title: Barco Callejero
layout: post
categories: []
---

Objetivos
---------

-   Crear un grupo de unas 5 o 6 personas que se junten semanalmente.
-   Generar ideas de actividades de difusión en la calle y convocatoria
    a marchas.
-   Llevar una agenda de actividades en la calle, marchas y demás.
-   Salir a panfletear a algunos lugares concretos (el
    \[Barco\_de\_Comunicación\|barco de propaganda\] se encarga de crear
    los panfletos, posters y comunicados)
-   Crecer y dividirse o fomentar otros barcos callejeros.
-   Coordinar con otros [barcos callejeros](Barco_Callejero "wikilink")
    para activar en las marchas.

[Actas](Actas:BarcoCallejeroSept/Oct "wikilink")
