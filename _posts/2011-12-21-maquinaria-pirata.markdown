---
title: Maquinaria Pirata
layout: post
categories: []
---

Siguiendo el modelo del [software](Programas_Piratas "wikilink") y la
cultura libre se esta empezando a crear hardware y planos de maquinas
libres para que vos puedas construir lo que de otra forma tendrias que
comprar a mucho mayor costo y que esta diseñado para volverse obsoleto
al poco tiempo.

Maquinaria
----------

-   [Open Source Ecology](http://opensourceecology.org/)
    [Wiki](http://opensourceecology.org/wiki)

Una red de granjeros, ingenieros y constructores creando las
herramientas basicas para la aldea global.

Circuitos/Hardware
------------------

-   [Arduino](http://www.arduino.cc/) Placas de circuitos y componentes
    de diseño libre para desarrollar proyectos de electronica a bajo
    costo.

Instalaciones
-------------

-   [Appropedia](http://www.appropedia.org) Soluciones colaborativas
    para el desarrollo sustentable.
