---
title: Translations:Carta Orgánica/32/es
layout: post
categories: []
---

Será responsabilidad del Partido propiciar el intercambio de
información, conocimiento y experiencia entre piratas mediante
festivales, talleres o cualquier tipo de evento abierto y gratuito.
