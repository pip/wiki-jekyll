---
title: Archivo:El becerro dorado.jpg
layout: post
categories: []
---

[El Becerro Dorado del
copyright](https://secure.flickr.com/photos/christopherdombres/4879506713/in/photostream)
\-- por [Christopher
Drombres](https://secure.flickr.com/photos/christopherdombres/),
[cc-by](http://creativecommons.org/licenses/by/2.0/deed.en)

[Category:Media](Category:Media "wikilink")
