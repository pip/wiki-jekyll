---
title: Translations:Carta Orgánica/18/es
layout: post
categories: []
---

Asimismo, será derecho de los Piratas Afiliados atender a las
actividades anteriormente citadas con voz, voto y participación activa,
de acuerdo a sus facultades individuales, ofrecimiento voluntario y/o
delegación asamblearia.
