---
title: Translations:Carta Orgánica/166/es
layout: post
categories: []
---

La Asamblea Permanente podrá solicitarle al Tesoro cualquier clase de
información adicional que considere necesaria.
