---
title: Manifiesto Nickname
layout: post
categories: []
---

En un principio estaba el nick y todos eramos felices viviendo en un
mundo de anonimato, nombres extraños y amistades virtuales - luego
surgieron las plataformas \"sociales\" y les confiamos nuestros datos
reales creyendo que éstas reconocerían el valor de los mismos, los
protegerían y los utilizarían en nuestro beneficio. Pero no lo hicieron.

El uso de los seudónimos para no divulgar la verdadera identidad, data
de hace siglos; ya sea por nombres artísticos, la simplificación de
nombres extranjeros, de difícil pronunciación, el temor al escándalo, o
la persecución política o religiosa \[1\].

Adentrando en el mundo informático se propago el uso de los seudónimos
en los canales de chat, los famosos \"nickname\", los cuales protegían
la verdadera identidad del usuario en la Red, pudiendo expresarse
libremente.

La explosión de las plataformas sociales, en los últimos años, terminó
imponiendo el uso obligatorio de Nombres y Apellidos, con los fines de
\"salvaguardar\" nuestra seguridad en la red; atentos a este \"acto
desinteresado\" por parte de las empresas que brindan servicios como la
plataformas sociales, e-mails, mensajería instantánea o salas de chat,
los usuarios revelamos nuestros datos personales y fuimos engañados:
aceptando acuerdos de usos finales impuestos por las empresas para la
utilización de sus servicios, en los cuales en la mayoría de los casos
damos nuestros consentimientos, para utilizar nuestra información
personal en sus negocios.

El uso del nick, no es la solución al gran problema de la privacidad en
la red, es solo un inicio de una larga y ardua campaña de
concientización sobre el derecho a la privacidad.

Hoy venimos a cuenta cuan ilusos hemos sido al confiarle este poder a
las plataformas sociales precisamente en esta época en que uno ya no
posee -ni la ilusión de- control sobre sus datos\[2\]\[3\], en que uno
puede ir a prisión por un tweet\[4\] o por pertenecer a un grupo en una
plataforma social\[5\], nos hemos convertido en un producto\[6\]\[7\] y
no en un usuario, proponemos una vuelta a esos tiempos en que nuestro
anonimato nos daba la Libertad de decir lo que quisiéramos sin miedo a
ser perseguidos y reprimidos: Proponemos un regreso inmediato al uso de
nicknames.

¡Que vuelvan los nicks!, es la campaña para empezar a decir no a todo
esto, por todo esto proponemos cambiar tu nombre por un seudónimo en
todas las plataformas sociales y sobre todo al hacer una nueva cuenta no
usar tu nombre real.

Referencias

\[1\]
[https://secure.wikimedia.org/wikipedia/es/wiki/Seudónimo](https://secure.wikimedia.org/wikipedia/es/wiki/Seudónimo)

\[2\] No podes borrar definitivamente tus datos
<http://es.kioskea.net/faq/2320-darse-de-baja-o-eliminar-cuenta-de-facebook>
(Como borrar la cuenta de Facebook ¿totalmente?)

\[3\] Google admite entregar informacion de usuarios a agencias de
inteligencia
<http://alt1040.com/2011/08/google-primera-compania-en-admitir-la-entrega-de-datos-de-sus-usuarios-europeos-a-las-agencias-de-inteligencia-de-estados-unidos>

\[4\] Arrestado por un tweet
<https://www.npr.org/blogs/thetwo-way/2010/11/12/131269198/-i-am-spartacus-man-convicted-for-tweet-virtual-protest-erupts>

\[5\] Condenado a 4 años por comentarios en Facebook
<http://www.cbsnews.com/stories/2011/08/17/501364/main20093364.shtml>

\[6\] Facebook planea vender tu informacion
<https://www.readwriteweb.com/archives/facebook_sells_your_data.php>

\[7\] Facebook y otros descubiertos vendiendo tu informacion
<http://mashable.com/2010/05/20/facebook-caught-sending-user-data-to-advertisers/>

1.  license:CC-by-nc-3.0
