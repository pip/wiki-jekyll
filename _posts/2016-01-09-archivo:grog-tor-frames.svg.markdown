---
title: Archivo:Grog&Tor frames.svg
layout: post
categories:
- Media
- Grog&Tor
---

[Categoría:Media](Categoría:Media "wikilink")
[Categoría:Grog&Tor](Categoría:Grog&Tor "wikilink")

La fuente es \"[The Secret Font of Monkey
Island](http://ptless.org/sfomi/)\". Cada frame fue exportado como PNG y
numero del 001 al 012.

Al importarlos en [aseprite](http://aseprite.org), ofreció cargar todos
los frames numerados.

Luego con el atajo \'p\' se le fueron poniendo los milisegundos y se lo
guardó como GIF:

[Imagen:Grog&Tor\_interdimensional.gif](Imagen:Grog&Tor_interdimensional.gif "wikilink")
