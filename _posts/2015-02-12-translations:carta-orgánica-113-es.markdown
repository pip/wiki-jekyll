---
title: Translations:Carta Orgánica/113/es
layout: post
categories: []
---

El incumplimiento será considerado falta grave y podrá acarrear como
sanción la expulsión del partido.
