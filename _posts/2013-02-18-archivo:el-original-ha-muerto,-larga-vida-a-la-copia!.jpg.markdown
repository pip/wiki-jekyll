---
title: Archivo:El original ha muerto, larga vida a la copia!.jpg
layout: post
categories: []
---

[El original ha
muerto](https://secure.flickr.com/photos/christopherdombres/4705127631/in/photostream)
\-- por [Christopher
Drombres](https://secure.flickr.com/photos/christopherdombres/),
[cc-by](http://creativecommons.org/licenses/by/2.0/deed.en)

[Category:Media](Category:Media "wikilink")
