---
title: Preguntas más frecuentes
layout: post
categories: []
---

Algunas de las Preguntas más frecuentes encontradas en la página del
Poder Judicial de la Nación

**1. ¿Qué tipos de partidos políticos existen en el orden nacional?** La
ley 23.298 prevé la existencia de partidos de distritos y partidos
nacionales y establece los requisitos que las agrupaciones deben
satisfacer para ser reconocidas como tales. El partido de distrito
cuenta con personalidad jurídico-política para actuar exclusivamente en
el distrito en el que es reconocido como tal. El partido nacional
-formado por un mínimo de cinco partidos de distrito- actúa en todos
aquéllos en los que se encuentra inscripto a través del mecanismo
previsto por el art. 8o de la ley 23.298. (cf. Fallo CNE 1258/92). La
distinción esencial radica en que solo los partidos nacionales pueden
postular candidatos a Presidente y Vicepresidente de la Nación, pues ese
reconocimiento los faculta para actuar en todo el ámbito nacional, el
cual constituye, a los efectos de la elección presidencial, un distrito
único (cf. Fallo 3110/03).

**2. ¿Cómo se constituye un partido político de distrito?** Para que a
una agrupación política se le pueda reconocer su personería
jurídico-política, en forma provisoria, debe solicitarlo ante el juez
competente, cumpliendo con los siguientes requisitos: · Acta de
fundación y de constitución, acompañada de constancias, que acrediten la
adhesión de un número de electores no inferior al cuatro por mil del
total de los inscriptos en el registro de electores del distrito
correspondiente, hasta el máximo de un millón (1.000.000). Este acuerdo
de voluntades se complementará con un documento en el que conste nombre,
domicilio y matrícula de los firmantes. · Nombre adoptado por la
asamblea de fundación y constitución · Declaración de principios y
programa o bases de acción política, sancionados por la asamblea de
fundación y constitución · Carta orgánica sancionada por la asamblea de
fundación y constitución · Acta de designación de las autoridades
promotoras · Domicilio partidario y acta de designación de los
apoderados Durante la vigencia del reconocimiento provisorio los
partidos políticos serán considerados en formación. No pueden presentar
candidaturas a cargos electivos en elecciones primarias ni en elecciones
nacionales, ni tienen derecho a aportes públicos ordinarios ni
extraordinarios. Para obtener la personería jurídico-política
definitiva, los partidos en formación deben acreditar: · Dentro de los
ciento cincuenta días, la afiliación de un número de electores no
inferior al cuatro por mil del total de los inscriptos en el registro de
electores del distrito correspondiente, hasta el máximo de un millón
acompañadas de copias de los documentos cívicos de los afiliados donde
conste la identidad y el domicilio, certificadas por autoridad
partidaria · Dentro de los ciento ochenta días, haber realizado las
elecciones internas, para constituir las autoridades definitivas del
partido · Dentro de los sesenta días de obtenido el reconocimiento,
haber presentado los libros a que se refiere el artículo 37, a los fines
de su rúbrica. Todos los trámites ante la justicia federal con
competencia electoral hasta la constitución definitiva de las autorices
partidarias serán efectuados por las autoridades promotoras, o los
apoderados, quienes serán solidariamente responsables de la veracidad de
lo expuesto en las respectiva documentaciones y presentaciones. (art. 7
y 7bis, ley 23.298)

**3. ¿Cómo se constituye un partido político nacional?** Los partidos de
distrito reconocidos que resuelvan actuar en cinco o más distritos como
partido nacional, además de lo preceptuado en la respuesta precedente,
deben cumplir con los siguientes requisitos: · Testimonio de la
resolución que le reconoce personalidad jurídica-política · Declaración
de principios, programa o bases de acción política y carta orgánica ·
Acta de designación y elección de las autoridades nacionales del partido
y de las autoridades de distrito · Domicilio partidario central y acta
de designación de los apoderados. (art. 8, ley 23.298)

**4. ¿Cuáles son las causas de pérdida de la personalidad de un partido
político?** El título VI de la ley 23.298 -"De la caducidad y extinción
de los partidos políticos"- alcanza tanto a los partidos de distrito
como a los nacionales. La caducidad da lugar a la cancelación de la
inscripción del partido en el registro y la pérdida de la personalidad
jurídica. La extinción importa el fin de la existencia legal del partido
y da lugar a su disolución. El partido sobre el que pesa una declaración
de caducidad mantiene su existencia legal --puesto que ésta solo se
pierde con la extinción- subsistiendo como persona jurídica de derecho
privado mientras no se produzca la disolución de la entidad.

**5. ¿Por qué razones puede caducar un partido político?** El artículo
50 de la ley 23.298 enumera taxativamente las causales de caducidad, que
son: · La no realización de elecciones partidarias internas durante el
término de 4 años; · La no presentación a dos elecciones nacionales
consecutivas; ·· No alcanzar en dos elecciones nacionales sucesivas el
dos por ciento del padrón electoral del distrito que corresponda; · La
violación de lo determinado en los artículos 7, incisos e) y 37 -de esta
ley-, previa intimación judicial. · No mantener la afiliación mínima
prevista por los artículos 7o y oter --ley 23.298-; · No estar integrado
un partido nacional por al menos cinco partidos de distrito con
personería vigente; · La violación a lo dispuesto en los incisos f) y g)
del artículo 33 -de esta ley-.

**6. ¿Por qué razones puede extinguirse un partido político?** Las
causales de extinción de los partidos políticos están enumeradas en el
artículo 51 de la ley 23.298, según el cual ello acontece: · Por las
razones que determine la Carta Orgánica; · Por voluntad de los
afiliados, expresada de acuerdo con la Carta Orgánica; · Cuando
autoridades del partido o candidatos no desautorizados por aquéllas,
cometieran delitos de acción pública; · Por impartir instrucción militar
a los afiliados u organizarlos militarmente.

**7. ¿Qué características presenta el régimen de financiamiento de los
partidos políticos en Argentina?** Las agrupaciones políticas mantienen
una doble vía de financiación, mediante la vigencia de un sistema
conocido como "dual" o "mixto", esto es, a través de aportes públicos y
privados, que procura un equilibrio tendiente a evitar la excesiva
dependencia de los partidos respecto del Estado -por un lado- y la
influencia de grupos de interés, o presión, sobre los partidos o
candidatos a los que apoyan, por el otro.

**8. ¿Qué norma regula el financiamiento de los partidos en Argentina?**
La ley de financiamiento de los partidos políticos no 26.215 --sustituta
de la ley n° 25.600- sancionada en el año 2006, es la que prevé lo
concerniente al financiamiento de la actividad partidaria. Entre sus
disposiciones regula materias como el financiamiento público y privado,
la Poder Judicial de la Nación publicidad de las cuentas de los
partidos, e impone limites de gastos y aportes. El control de las
finanzas partidarias lo atribuye a la justicia nacional electoral.

**9. ¿Cómo se canaliza el financiamiento privado?** Los aportes privados
pueden ser destinados al Fondo Partidario Permanente, a cargo del
Ministerio del Interior, o directamente a los partidos políticos. Las
contribuciones o donaciones realizadas por personas físicas o jurídicas
al Fondo Partidario Permanente o al partido político directamente son
deducibles para el impuesto a las ganancias hasta el límite del cinco
por ciento de la ganancia neta del ejercicio. (art. 17, ley 26.215)

**10. ¿Pueden los Partidos Políticos recibir cualquier tipo de
donaciones o contribuciones?** No, la ley contempla diversas
limitaciones. Así, no pueden recibir o aceptar -directa o
indirectamente- contribuciones o donaciones: · Anónimas; · De entidades
centralizadas o descentralizadas, nacionales, provinciales,
interestaduales, binacionales o multilaterales, municipales o de la
Ciudad de Buenos Aires; · De empresas concesionarias de servicios u
obras públicas de la Nación, las provincias, los municipios o la Ciudad
de Buenos Aires; · De personas personas físicas o jurídicas que exploten
juegos de azar; · De gobiernos o entidades públicas extranjeras; · De
personas físicas o jurídicas extranjeras que no tengan residencia o
domicilio en el país; · De personas que hubieran sido obligadas a
efectuar la contribución por sus superiores jerárquicos o empleadores; ·
De asociaciones sindicales, patronales o profesionales. Dichas
restricciones comprenden también a los aportes privados destinados al
Fondo Partidario Permanente. (art. 15, ley 26.215)

**11. ¿Existen límites a los fondos privados que los partidos políticos
pueden percibir?** Los partidos políticos no pueden recibir por año
calendario contribuciones o donaciones de: · una persona jurídica,
superiores al monto equivalente al 1% (uno por ciento) del total de
gastos permitidos; · una persona física, superiores al monto equivalente
al 2% (dos por ciento) del total de gastos permitidos. Los porcentajes
se computan sobre el límite de gastos establecido por el artículo 45 de
la ley 26.215. Con motivo de la campaña electoral, los partidos y
alianzas no pueden recibir un total de recursos privados que supere el
monto equivalente a la diferencia entre el tope máximo de gastos de
campaña y el monto del aporte extraordinario -para esa campaña
electoral- correspondiente al partido o alianza. (arts. 16 y 44, ley
26.215)

**12. ¿Cuáles son los límites de gastos de campaña?** En las elecciones
nacionales, los gastos destinados a la campaña electoral para cada
categoría que realice una agrupación política, no podrán superar, la
suma resultante al multiplicar el número de electores habilitados, por
un (1) módulo electoral de acuerdo al valor establecido en la Ley de
Presupuesto General de la Administración Nacional del año respectivo. A
efectos de la aplicación de lo precedentemente señalado, se considerará
que ningún distrito tiene menos de quinientos mil (500.000) electores.
El límite de gastos previsto para la segunda vuelta será la mitad de los
previsto para la primera vuelta. (art. 45, ley 26.215)

**13. ¿Cómo rinden cuentas de sus finanzas las agrupaciones políticas?**
Dentro de los noventa días de finalizado cada ejercicio anual deben
presentar ante el juez federal con competencia electoral de su distrito,
el estado anual de su patrimonio o balance general y la cuenta de
ingresos y egresos del ejercicio, poniendo a disposición la
correspondiente documentación respaldatoria. En cuanto al control de la
campaña electoral, la ley dispone que diez días antes de la celebración
de los comicios y noventa días después de finalizados, el presidente y
el tesorero del partido, junto con los responsables económico-financiero
y político de la campaña deben presentar un informe detallado de los
aportes públicos y privados recibidos -indicando origen y monto- así
como los gastos incurridos con motivo de la campaña. Asimismo, deben
indicar la fecha de apertura y cierre de la cuenta bancaria abierta para
la campaña electoral para el caso de las alianzas electorales. (arts.
23, 54 y 58, ley 26.215)

**14. ¿Los informes financieros partidarios son de acceso público?
¿Dónde pueden consultarse?** La ley 26.215 reglamenta el art. 38 de la
Constitución Nacional, en cuanto establece que "los partidos políticos
deberán dar publicidad del origen y destino de sus fondos y patrimonio".
A tal fin encomienda a las agrupaciones políticas la carga de facilitar
la consulta en "Internet" de todos los datos e informes que se deben
presentar, teniendo así carácter público para ser consultados libremente
por cualquier interesado. Sin perjuicio de ello, la Cámara Nacional
Electoral dispuso -en pos de asegurar la plena vigencia del principio
republicano que impone la publicidad de los actos de gobierno- que los
jueces electorales de todo el país publiquen el sitio web del poder
judicial de la nación los informes financieros que presenten los
partidos políticos. Asimismo, cualquier ciudadano puede solicitar en la
sede del juzgado -sin expresión de causacopia de los informes
presentados, quedando a su cargo el costo de las copias. (arts. 24 y 25,
ley 26.215 y Acordada 58/02 CEN)

**15. ¿Qué se entiende por Campaña Electoral?** El Código Electoral
Nacional denomina así al conjunto de actividades realizadas con el
propósito de promover o desalentar expresamente la captación del
sufragio a favor o en contra de candidatos a cargos públicos electivos
nacionales. (art. 64 bis CEN)

**16. ¿Cuáles son los términos y condiciones para realizar actos de
campaña?** La campaña electoral sólo puede iniciarse treinta y cinco
(35) días antes de la fecha fijada para los comicios y debe finalizar
cuarenta y ocho (48) horas antes del inicio del comicio. Sin embargo, la
publicidad en medios de comunicación --televisivos, radiales y gráficos-
no puede iniciarse, en ningún caso, antes de los veinticinco (25) días
previos a la fecha fijada para el acto electoral. Asimismo, durante el
período de campaña electoral, está prohibida la publicidad de actos de
gobierno que contenga elementos que promuevan la captación del sufragio
a favor de alguno de los candidatos. Finalmente, esta prohibido durante
los quince (15) días anteriores a la fecha fijada para la celebración de
las primarias, abiertas, simultáneas y obligatorias y la elección
general, la realización de actos inaugurales de obras públicas, el
lanzamiento o promoción de planes, proyectos o programas de alcance
colectivo y, en general, la realización de todo acto de gobierno que
pueda promover la captación del sufragio a favor de cualquiera de los
candidatos a cargos públicos electivos nacionales (arts. 64 bis, ter,
quater y 133 bis CEN)

**17. ¿Qué sanción existe para quien incumpla con las prohibiciones
referentes a la publicidad en medios de comunicación?** · El partido
político que incumpla los límites de emisión y publicación de avisos
publicitarios en medios de comunicación pierde el derecho a recibir
contribuciones, subsidios y todo recurso de financiamiento público
anual, por el plazo de uno a cuatro años y los fondos para el
financiamiento de campaña por una o dos elecciones. · En caso de que sea
una persona física o jurídica, será pasible de una multa de entre 10.000
(diez) mil y 100.000 (cien mil) pesos. · La persona física o jurídica
que explote un medio de comunicación y viole la prohibición es pasible
de: - Multa equivalente al valor total de los segundos de publicidad de
uno hasta cuatro días, conforme a la facturación del medio en el mes
anterior a aquel en que se produce la infracción, si se trata de un
medio televisivo o radial. - Multa equivalente al valor total de los
centímetros de publicidad de uno a cuatro días, conforme a la
facturación del medio en el mes anterior a aquél en que se produzca la
infracción, si se trata de un medio gráfico. (art. 128 ter CEN)

**18. ¿En qué período está vedada la realización de actos de
proselitismo, y la publicación o difusión de encuestas y sondeos de
opinión? ¿Qué sanción se prevé para el caso de incumplimiento?** Según
lo establece el Código Electoral Nacional, quien realice actos públicos
de proselitismo, publique y difunda encuestas o sondeos preelectorales,
desde cuarenta y ocho horas antes de la iniciación del comicio y hasta
su cierre, es pasible de una multa de entre 10.000 (diez mil) y 100.000
(cien mil) pesos. Idéntica sanción se prevé para quien publique o
difunda encuestas y proyecciones sobre el resultado de la elección
durante la realización del comicio y hasta tres horas después de su
cierre. (art. 128 bis CEN, modif. por ley 25.610

**19. ¿Cómo se registran los candidatos?** Desde la proclamación de los
candidatos en las elecciones primarias y hasta cincuenta (50) días
anteriores a la elección, los partidos registrarán ante el juez
electoral las listas de los candidatos proclamados. En el caso de la
elección del presidente y vicepresidente de la Nación, la representación
de las fórmulas de candidatos se realizará ante el juez federal con
competencia electoral de la Capital Federal. (art. 60 CEN)
