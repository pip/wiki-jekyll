---
title: Barca Feminista
layout: post
categories: []
---

Objetivos
---------

`* Aportar a la lucha contra el machismo instalado en un mundo que criminaliza, violenta, reprime y discrimina a las mujeres y otras identidades no hegemónicas.`\
`* Generar material de difusión para Utopías Piratas.`

Ejes de trabajo
---------------

`* Derecho a decidir sobre nuestro propio cuerpo`\
`* Horizontalidad y herramientas de autogestión que apunten al empoderamiento de las mujeres y otras identidades.`\
`* Violencia de géneros.`\
`* Lenguaje machista.`\
`* Desigualdad de géneros, informática y nuevas tecnología.`

Actas
-----

¿Qué leemos? ¿Qué miramos?
--------------------------

`* `[`1`](http://argentina.indymedia.org/uploads/2012/04/manual_aborto_con_pastillas.pdf)\
`* `[`2`](http://apps.who.int/iris/bitstream/10665/134747/1/9789243548715_spa.pdf?ua=1&ua=1)\
`* `[`3`](http://mujeresenred.net/)\
`* `[`4`](http://pillku.org/)

¿Cómo nos organizamos?
----------------------

`* Reuniones acordadas con anticipación en lo posible`\
`* Lista de mail de discusión >> [barcofeminista@asambleas.partidopirata.com.ar]`\
`* Suscripciones >> `[`5`](http://asambleas.partidopirata.com.ar/listinfo/barcofeminista)` (preferimos que nos mandes un mail antes)`\
`* Telegram`\
`* Por comenzar a usar GPG para comunicaciones seguras`\
`* Loomio/Consenso >> `[`6`](https://consenso.partidopirata.com.ar/g/FNyGpclD/barca-feminista)
