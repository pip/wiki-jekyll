---
title: Translations:Carta Orgánica/88/es
layout: post
categories: []
---

Será obligación de los Órganos Burocráticos acatar la voluntad de las
Asambleas Piratas siguiendo los mecanismos definidos en el Título III de
este documento y elaborar Actas de sus actividades.
