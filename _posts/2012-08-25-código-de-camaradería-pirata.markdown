---
title: Código de camaradería pirata
layout: post
categories: []
---

El único capital que tenemos como piratas son los piratas, sus
conocimientos y sus capacidades. Preservemoslas manteniendo una buena
relación entre nosotros.

Todos los piratas son buenos
----------------------------

Para toda discusión siempre se supone que los demas actuan de buena fé y
que participa por verdadero interes pirata. Nadie es un infiltrado de la
campora, la side, la cia o los masones reptilianos.

Argumentos ad hominem
---------------------

Evitemos las descalificaciones personales, se debaten las ideas no las
personas.

Que no se vaya Jose María
-------------------------

El intercambio por email es ineficiente. Muchas veces no nos conocemos,
el sentido, la entonación y demás sutilezas de la comunicación pueden
perderse entre que escribimos el email y lo lee la otra persona.

Por eso tratemos de ser claros al escribir, pensemos que la otra persona
puede no conocernos y malinterpretarnos o tomar lo que escribimos con
otro sentido. Y cuando leemos recordemos el primer punto \"todos los
piratas son buenos\".
