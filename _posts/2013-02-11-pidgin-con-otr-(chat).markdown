---
title: Pidgin con OTR (Chat)
layout: post
categories: []
---

![](001_pidgin.png "001_pidgin.png")

![](002_xmpp.png "002_xmpp.png")

![](003_cuentas.png "003_cuentas.png")

![](004_piratas.png "004_piratas.png")

![](005_complementos.png "005_complementos.png")

![](006_otr.png "006_otr.png")

![](007_conversacion.png "007_conversacion.png")

![](008_iniciar_otr.png "008_iniciar_otr.png")

![](009_generar_llave.png "009_generar_llave.png")

![](010_autenticar.png "010_autenticar.png")

![](011_pregunta.png "011_pregunta.png")

![](012_secreto.png "012_secreto.png")

![](013_huella.png "013_huella.png")

![](014_privado.png "014_privado.png")

![](015_autenticados.png "015_autenticados.png")
