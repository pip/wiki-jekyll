---
title: Complementos Para Firefox
layout: post
categories: []
---

¿Como instalar complementos?
============================

Podes instalar complementos desde la página de este en Mozilla
([Ejemplo](https://addons.mozilla.org/en-US/firefox/addon/ghostery/))
dandole al boton verde.

O desde el mismo navegador yendo a *Herramientas -\> Complementos -\>
Buscar complementos* y buscando el nombre del complemento que queremos
instalar.

Una vez instalados deberian aparecer en *Complementos -\> Extensiones* y
desde ahi mismo se pueden desinstalar.

No instrusivos
==============

Luego de instalar estos complementos **todo deberia funcionar
normalmente**.

-   [HTTPSEverywhere](https://www.eff.org/https-everywhere/) Conectate
    de forma segura a los sitios web.

<!-- -->

-   [Perspectives\_(Navegación\_Segura)](Perspectives_(Navegación_Segura) "wikilink")
    es una red de servidores que actúan como \"testigos\" de los
    certificados seguros de los sitios web.

<!-- -->

-   [AdBlock Plus](https://adblockplus.org) bloquea las molestas
    publicidades que te rastrean en Internet.

<!-- -->

-   [Ghostery](https://www.ghostery.com) bloquea el analisis de
    comportamiento que te rastrea en Internet.

Intrusivos
==========

Luego de instalar estos complementos **algunos sitios podrían no
funcionar correctamente** hasta que hayas configurado el complemento.

-   [NoScript](https://addons.mozilla.org/en-US/firefox/addon/noscript/)
    Bloquea todo el JavaScript y los objetos Flash, salvo aquellos que
    vos permitas. Sirve para evitar el rastreo y virus cuando visitas
    sitios desconocidos.

[Category:Privacidad](Category:Privacidad "wikilink")
[Category:Software\_Libre](Category:Software_Libre "wikilink")
