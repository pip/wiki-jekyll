---
title: Manifiesto WikiPartido Pirata Mexicano
layout: post
categories: []
---

Educación, cultura libre, información libre, respeto y protección a la
privacidad de los ciudadanos, transparencia y rendición de cuentas por
parte del gobierno, garantías de acceso y neutralidad para la red,
combate a los monopolios, reforma de la legislación actual de derechos
de autor, propiedad intelectual y patentes, protección del dominio
público.

El PPMX es un proyecto de partido ciudadano para la sociedad de la
información mexicana . Para un pirata, la cultura, el conocimiento, y
todo aquello que sea útil para el desarrollo social debe ser considerado
un procomún, por lo que aquellos mecanismos que mantienen lo anterior en
el orden de lo privado deben ser replanteados, reestructurados,
reformados o si así se amerita, eliminados.

Nuestros objetivos principales son garantizar que se cumpla el derecho
constitucional de todos los mexicanos a la educación, a la cultura, al
libre flujo de información, a la privacidad, a la libertad.

Un pirata exige una verdadera democracia, justicia social y libertad
para todos los mexicanxs.

Queremos, apoyámos y promovemos una reforma total del sistema de
propiedad intelectual y patentes, ya que la actual legislación deja a un
lado el bienestar social y la protección de los bienes comunes, para
beneficiar los intereses de lucro de monopolios culturales, científicos
y de telecomunicaciones.

Queremos se creen leyes que garanticen la neutralidad de la red y el
acceso a ella, como fundamento básico para la re-construcción de una
verdadera democracia para la sociedad mexicana en la era de la
información.

Apoyamos y colaboramos con mecanismos y movimientos que promueven la
transparencia y rendición de cuentas por parte del gobierno, así como la
participación directa de los ciudadanos en la gobernabilidad del país.

Los piratxs mexicanxs, somos libres pensadores y enfocamos nuestros
esfuerzos a discutir, crear, proponer, promover y compartir nuevas
propuestas, modelos y actitudes políticas, que contribuyan a la creación
de una sociedad más justa y preparada para destruír la brecha digital en
el Siglo XXI, por medio de herramientas de acceso y uso libre, para la
distribución de información y el conocimiento libre.
