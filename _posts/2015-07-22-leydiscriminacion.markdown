---
title: LeyDiscriminacion
layout: post
categories: []
---

Proyecto de Ley Antidiscriminación: una supuesta solución que amplia los problemas
==================================================================================

> La defensa de la libertad de los peores es imprescindible para ampliar
> los derechos de todos. Cels Argentina[^1].

Por Beatriz Busaniche

Una vez más, bajo la fundada defensa de causas nobles, nos encontramos
con proyectos legislativos que difícilmente logren paliar el mal que
pretenden erradicar y que a su vez generan nuevos problemas. Varios
proyectos nos han convocado en la misma tónica: uno de ellos, el
proyecto antidiscriminación presentado por el Diputado Remo Carlotto y
la Diputada Diana Conti es un claro ejemplo, que a su vez está integrado
en este nuevo proyecto que avanza en la Cámara Baja.

El dictamen aprobado por la Comisión de Derechos Humanos y Garantías de
la Cámara de Diputados de la Nación integra textos de diversos
proyectos, entre ellos el de Carlotto y Conti, así como el expediente
9064-D-14 del Diputado Larroque y el expediente 2447-D-15 del Diputado
Heller[^2].

La propuesta apunta a actualizar y ampliar el alcance de la ley
antidiscriminación vigente, la Nro. 23.592 que rige desde el año
1988[^3]. Con este cometido, el proyecto amplía el concepto de acto
discriminatorio e incorpora algunas esferas tales como Internet y los
medios de comunicación en el alcance de la normativa.

Como suele ocurrir en estos casos, el objeto y los principios que rigen
la propuesta legal son incuestionables:

> ARTICULO 2°: OBJETO. La presente ley tiene por objeto promover y
> garantizar el principio de igualdad y no discriminación, en procura de
> la realización del conjunto de los derechos humanos, mediante la
> promoción, implementación y el desarrollo de políticas públicas
> inclusivas que fomenten el respeto por la diversidad, garanticen el
> derecho a la igualdad, el acceso a la justicia, y generen condiciones
> aptas para sancionar y erradicar toda forma de discriminación.

> ARTICULO 3°: PRINCIPIOS QUE RIGEN LA PRESENTE LEY. Esta ley garantiza
> todos los derechos reconocidos por los tratados que, en materia de
> derechos humanos, la República Argentina ha suscripto y se rige por
> los siguientes principios: - todas las personas nacen libres e iguales
> en dignidad y derechos y son iguales ante la ley y tienen derecho a
> una misma protección legal y efectiva contra la discriminación; -
> todas las personas tienen derecho a participar en cualquier área de la
> vida social, civil, cultural, política y económica en igualdad de
> oportunidades; - se reconoce a la diversidad y a la pluralidad como
> principios enriquecedores de las identidades, promoviendo la vigencia
> de estos principios en todos los ámbitos de la vida; - se reconocen a
> la inclusión y a la democracia como principios fundantes de todo
> proceso tendiente a garantizar la igualdad, reafirmando su carácter
> esencial para la prevención y la eliminación efectiva de toda forma de
> discriminación; - se reconoce y valora el respeto por la
> interculturalidad, interreligiosidad, perspectiva generacional,
> perspectiva de género, diversidad afectivo-sexual y perspectiva
> socioeconómica de la pobreza.

Sin embargo, las buenas intenciones no alcanzan a redimir la
contradicción en la que incurre el los artículos subsiguientes. Cuando
se trata de proponer medidas tendientes a erradicar la discriminación en
Internet, y considerando la amplitud inmensa de la definición de actos
discriminatorios, aparece una tensión elocuente con un derecho
fundamental también amparado en los tratados internacionales de Derechos
Humanos de los que Argentina es firmante, el derecho a la libertad de
expresión. Cuando revisamos la incorporación de los medios e Internet a
la norma en el artículo 21 nos encontramos con un texto que bien podría
incentivar la censura privada.

> ARTICULO 21°: PROMOCIóN DE LA NO DISCRIMINACIóN EN INTERNET. Los
> administradores de sitios de internet que dispongan de plataformas que
> admitan contenidos y/o comentarios subidos por los usuarios están
> obligados a: a) publicar términos y condiciones que contengan la
> información del Anexo II de esta ley, con el objeto de informar sobre
> el carácter discriminatorio de un contenido y la legislación vigente
> al respecto; b) disponer y hacer pública una vía de comunicación para
> que los usuarios denuncien y/o soliciten la remoción del material que
> se encuentre en infracción a esta ley. Los medios de prensa, agencia
> de noticias, diarios online y revistas electrónicas que cuenten con
> plataformas que admitan contenidos generados por los usuarios deben,
> además de las obligaciones previstas precedentemente, disponer de la
> información prevista en el inciso a) de este artículo a través de la
> activación automática de una ventana cuyos términos deben ser
> aceptados por el usuario antes de acceder a realizar el comentario o
> subir cualquier contenido, y *adoptar las medidas necesarias para
> evitar la* *difusión de contenidos discriminatorios*[^4].

En la definición que la norma hace de plataformas que habilitan
comentarios de los usuarios se incluyen páginas, blogs, redes sociales,
agencias de noticias, medios de prensa, diarios online, revistas
electrónicas y otros sitios de internet que admiten que los usuarios
publiquen contenidos, opiniones o dejen mensajes en sus respectivos
dominios[^5].

La regulación pretende alcanzar un número demasiado amplio de medios de
comunicación, incluyendo blogs personales y cualquier otro sitio que
permita comentarios de usuarios, una práctica extremandamente común en
la internet de hoy en día. Algunos de ellos, especialmente los que
cuenten con equipos de abogados o puedan darse el lujo de contratar
moderadores, podrán lidiar con la amenaza eliminando contenidos de
manera inmediata. Otros sólo tendrán el recurso de cerrar sus
comentarios, en una abierta reducción de los espacios de debate y
discusión hoy disponibles.

Si a esta amplitud en el alcance sumamos la definición de contenidos
discriminatorios del anexo II de la norma propuesta, nos encontramos con
que casi cualquier comentario que pueda ofender a una persona está
contemplado en la norma. La definición de contenido discriminatorio
incluida en el Anexo II expresa:

> CONTENIDO DISCRIMINADOR. Será considerado contenido discriminador
> aquellos mensajes publicados en las plataformas de contenidos
> producidos por los usuarios que menoscaben o insulten a las personas o
> grupo de personas o asociaciones, motivadas en la falsa noción de
> raza, así como en las nociones de etnia, nacionalidad, lengua, idioma
> o variedad lingüística, religión o creencia, ideología, opinión
> política o gremial, sexo, orientación sexual, género, identidad de
> género y/o su expresión, edad, color de piel, estado civil, situación
> familiar, filiación, embarazo, discapacidad, responsabilidad familiar,
> antecedentes penales, trabajo u ocupación, lugar de residencia,
> caracteres físicos, características genéticas, capacidad psicofísica y
> condiciones de salud, posición económica o condición social, hábitos
> personales o cualquier circunstancia que implique distinción,
> exclusión, restricción o preferencia. La presente enumeración no es
> taxativa y el carácter discriminador deberá ser evaluado con arreglo a
> la ley \.... de Actos Discriminatorios y los instrumentos
> internacionales de derechos humanos suscriptos por nuestro país.

A la mencionada amplitud de medios alcanzados y de discursos
contemplados se suma la incorporación de figuras penales.

> ARTICULO 23°.- Será reprimido con prisión de UN (1) mes a TRES (3)
> años quien: a) por cualquier medio alentare o incitare a la
> persecución, el odio, la violencia o la discriminación contra una
> persona o grupo de personas por los motivos enunciados en el artículo
> anterior; b) en forma pública u oculta, formare parte de una
> organización o realizare propaganda, basados en ideas o teorías de
> superioridad o inferioridad de un grupo de personas, que tengan por
> objeto la justificación o promoción de la discriminación por los
> motivos enunciados en el artículo anterior; c) en forma pública u
> oculta, financiare o prestare cualquier otra forma de asistencia a las
> organizaciones y actividades mencionadas en los incisos a) y b).

La regulación expresa que los medios de comunicación y los sitios de
internet *deberán adoptar las medidas necesarias para evitar la difusión
de contenidos discriminatorios*, pero no define el alcance de las
responsabilidades y la modalidad que deben tener estas medidas, mientras
que la definición de contenidos discriminatorios supera ampliamente lo
admitido en el contexto de los tratados internacionales de Derechos
Humanos.

Un marco difuso de responsabilidad de intermediarios sólo contribuye
como incentivo a la censura privada, ya que ningún medio de comunicación
o titular de plataforma de contenidos web querrá asumir el riesgo de ser
acusado civil o penalmente por la mera defensa de los comentarios de sus
usuarios. El sistema de incentivos a la censura privada opera de este
modo: si el titular de la plataforma es responsable por lo que dicen los
usuarios, seguramente la baja de contenidos será moneda corriente, tal
como de forma elocuente advirtieron los relatores de libertad de
expresión cuando en 2011 publicaron su documento sobre Libertad de
Expresión en Internet[^6].

Adaptar la norma a los Derechos Humanos
---------------------------------------

El informe \"Libertad de Expresión e Internet\" de la Relatoría especial
para la Libertad de Expresión de la Comisión Interamericana de Derechos
Humanos nos ofrece un marco claro de definiciones a fin de revisar y
modificar el proyecto propuesto[^7].

> 85. En casos excepcionales, cuando se está frente a contenidos
>     abiertamente ilícitos o a discursos no resguardados por el derecho
>     a la libertad de expresión (como la propaganda de guerra y la
>     apología del odio que constituya incitación a la violencia, la
>     incitación directa y pública al genocidio, y la pornografía
>     infantil) resulta admisible la adopción de medidas obligatorias de
>     bloqueo y filtrado de contenidos específicos. En estos casos, la
>     medida debe someterse a un estricto juicio de proporcionalidad y
>     estar cuidadosamente diseñada y claramente limitada de forma tal
>     que no alcance a discursos legítimos que merecen protección. En
>     otras palabras, las medidas de filtrado o bloqueo deben diseñarse
>     y aplicarse de modo tal que impacten, exclusivamente, el contenido
>     reputado ilegítimo, sin afectar otros contenidos<ref>
>     Véase
>     <https://www.oas.org/es/cidh/expresion/docs/informes/2014_04_08_Internet_WEB.pdf>
>
>     </ref>.
>
En este sentido, la amplitud de la definición de contenidos
discriminatorios excede ampliamente las excepciones al discurso
protegido por la libertad de expresión en los términos del artículo 13
de la Convención Americana. A esto se suma el hecho de que la autoridad
de aplicación de la norma es el Poder Ejecutivo Nacional, en abierta
contradicción con el párrafo 86 del mismo documento de la

CIDH que expresa:

> 86. En los casos excepcionales mencionados, la Relatoría Especial
>     considera que las medidas deben ser autorizadas o impuestas
>     atendiendo a las garantías procesales, según los términos de los
>     artículos 8 y 25 de la Convención Americana. En este sentido, las
>     medidas solamente deberán ser adoptadas previa la plena y clara
>     identificación del contenido ilícito que debe ser bloqueado, y
>     cuando la medida sea necesaria para el logro de una finalidad
>     imperativa. En todo caso, la medida no debe ser extendida a
>     contenidos lícitos.

Por otro lado, la cláusula de la ley que obliga a los prestadores de
servicios de Internet y plataformas web a impedir la circulación de
contenidos discriminatorios colisiona con la recomendación de la
Relatoría de Libertad de Expresión de CIDH que expresa:

> 88. En ningún caso se puede imponer una medida ex-ante que impida la
>     circulación de cualquier contenido que tenga presunción de
>     cobertura. Los sistemas de filtrado de contenidos impuestos por
>     gobiernos o proveedores de servicios comerciales que no sean
>     controlados por el usuario final constituyen una forma de censura
>     previa y no representan una restricción justificada a la libertad
>     de expresión.

En relación a la responsabilidad de los intermediarios de Internet, vale
mencionar, en línea con la CIDH que:

> 99. \..., en la mayoría de los casos, los intermediarios no tienen -ni
>     tienen que tener - la capacidad operativa/técnica para revisar los
>     contenidos de los cuales no son responsables. Tampoco tienen - ni
>     tienen que tener- el conocimiento jurídico necesario para
>     identificar en qué casos un determinado contenido puede
>     efectivamente producir un daño antijurídico que debe ser evitado.
>     Pero incluso si contaran con el número de operadores y abogados
>     que les permitiera realizar este ejercicio, los intermediarios, en
>     tanto actores privados, no necesariamente van a considerar el
>     valor de la libertad de expresión al tomar decisiones sobre
>     contenidos producidos por terceros que pueden comprometer su
>     responsabilidad.

Conclusiones y recomendaciones
------------------------------

A finales de la década del 40, cuando los redactores de la Declaración
Universal de Derechos Humanos tuvieron que definir y establecer el
alcance de la libertad de expresión, tenían en sus espaldas el fantasma
de una de las mayores tragedias que la humanidad haya vivido: el
holocausto y el nazismo. En ese contexto, los entonces delegados de la
URSS proponían como límite a la libertad de expresión todo discurso
racista y nazi. La delegación de Bolivia, encabezada por Eduardo Anze
Matienzo expresó que la \"forma segura de curar esos males (en
referencia al nazismo y el fascismo) era asegurar las libertades
fundamentales\"[^8]. Es esta la tónica que debemos imprimir en una
normativa que pretenda salvaguardar el derecho a la igualdad de toda
persona y el derecho a no ser discriminado por ninguna razón.

La amplitud en la definición de contenidos discriminatorios, la
incorporación de plataformas y servicios de Internet, la inclusión de
tipos penales y la no definición clara de la responsabilidad de
intermediarios nos llevan a concluir que el proyecto de Ley Nacional
Antidiscriminación colisiona con los principios de proporcionalidad
indispensables en toda regulación sobre el discurso público.

Por esta razón, instamos a los legisladores a revisar los conceptos allí
expresados, en particular, a

1.  acotar el alcance de la definición de contenidos discriminatorios a
    los parámetros ya determinados por la Convención Americana de
    Derechos Humanos,
2.  establecer con claridad los procedimientos y los alcances de las
    medidas de eliminación, filtrado y bloqueo de contenidos y
3.  excluir a los proveedores de servicios de internet, sitios web y
    otras plataformas de la tarea de limitar la circulación de
    contenidos discriminatorios exhimiendo a estos actores de
    responsabilidad en la materia.
4.  establecer resguardos judiciales para la baja de contenidos, bajo
    condiciones claramente establecidas en el marco de la ley.

Es fundamental que quede explícitamente claro, en sintonía con la
jurisprudencia argentina, que los responsables de los sitios de internet
y titulares de medios de comunicación no tienen obligación de vigilar lo
que publican sus usuarios y que son responsables por los contenidos
producidos por ellos salvo orden judicial, ya que esto generaría un
incentivo directo a la censura privada.

El texto de la ley tal como está expresado actualmente no parece ser la
solución apropiada frente a la problemática de la discriminación y el
discurso de odio. Más bien, se constituye en una amenaza clara a la
libertad de expresión.

<references />

[^1]: Véase [Documento del CELS sobre los proyectos de ley
    antidiscriminación de los diputados Remo Carlotto y Diana Conti.
    Visitado el 16 de Julio de
    2015.](http://cels.org.ar/common/documentos/CELS%20sobre%20proyecto%20de%20ley%207379-D-2014%20final%20(1).pdf)

[^2]: Véase dictamen de comisión de DDHH y Garantías, disponible en
    <http://www.vialibre.org.ar/wp-content/uploads/2015/07/DICTAMEN-ACTOS-DISCRIMINATORIOS-Final.pdf>

[^3]: Véase
    <http://www.infoleg.gov.ar/infolegInternet/anexos/20000-24999/20465/texact.htm>

[^4]: El destacado es nuestro.

[^5]: Véase Anexo I de la norma propuesta.

[^6]: Véase la Declaración Conjunta sobre Libertad de Expresión e
    Internet en
    <https://www.oas.org/es/cidh/expresion/showarticle.asp?artID=848>

[^7]: Véase
    <https://www.oas.org/es/cidh/expresion/docs/informes/2014_04_08_Internet_WEB.pdf>

[^8]: MORSNIK, J. (1999) The Universal Declaration of Human Rights.
    Origins, Drafting and Intent. Philadelphia, Pennsylvania: University
    of Pennsylvania Press. p. 69.
