---
title: Translations:Carta Orgánica/128/es
layout: post
categories: []
---

Dicha cuenta bancaria debe tener el carácter de compartida, esto es que
para realizar movimiento de fondos se requiera autorización expresa de
todos quienes hubiesen sido registrados a tal fin.
