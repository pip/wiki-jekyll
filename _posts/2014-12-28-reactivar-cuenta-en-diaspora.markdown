---
title: Reactivar cuenta en Diaspora
layout: post
categories: []
---

Visto que:

La cuenta en Diáspora (https://diaspora.com.ar/people/8300d9786dd88cbf)
hace 4 años que no tiene uso, desconozco los motivos por los que se
insiste en usar Facebook siendo que es uno de los sitios preferidos al
atacar a quienes no respetan la privacidad.

Propongo:

1\) Nombrarme a mi responsable de publicaciones en Diáspora 2) Si por
motivos de marketing es necesario seguir usando la cuenta de Facebook,
puede utilizarse la integración con Diáspora para publicar en simultaneo
(en este caso no haría falta el punto 1. 3) Armar un Barco para esta
tarea (o coordinar con alguno ya existente). No me creo capacitado para
responder comentarios.

Publíquese.
