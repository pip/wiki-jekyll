---
title: Plantilla:Atención
layout: post
categories: []
---

  ------------------------------------------------------------------------ --
  ![](Dialog-warning-orange.svg "Dialog-warning-orange.svg"){width="55"}   
  ------------------------------------------------------------------------ --

<noinclude>Uso: {{Atención \|texto=\"Contenido del recuadro\"}}
</noinclude>
