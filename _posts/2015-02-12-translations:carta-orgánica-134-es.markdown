---
title: Translations:Carta Orgánica/134/es
layout: post
categories: []
---

El Órgano Fiduciario deberá responder a la Asamblea, si mediante la
misma los Piratas solicitacen realizar un gasto de dinero superior al
monto disponible para la caja chica.
