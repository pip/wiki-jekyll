---
title: Translations:Carta Orgánica/69/es
layout: post
categories: []
---

Las Asambleas Generales o Flotas Piratas son asambleas presenciales con
participación digital opcional realizadas con periodicidad según
reglamento.
