---
title: Perspectives (Navegación Segura)
layout: post
categories:
- Privacidad
- Seguridad Digital
- Software Libre
---

[Categoría:Privacidad](Categoría:Privacidad "wikilink")
[Categoría:Seguridad Digital](Categoría:Seguridad_Digital "wikilink")
[Categoría:Software Libre](Categoría:Software_Libre "wikilink")

Si bien los certificados seguros (SSL) nos permiten cifrar las
conexiones a sitios web a la vez que identificarlos, es posible que un
agente malicioso logre hacerse pasar por ese sitio.

[Perspectives](https://perspectives-project.org) es una red de
servidores que actúan como \"testigos\" de los certificados seguros de
los sitios web. Al usar el plugin para [navegadores
Mozilla](https://addons.mozilla.org/en-US/firefox/addon/perspectives/),
le preguntamos a varios testigos cuál certificado vieron para el sitio
que queremos visitar. Si todos vieron el mismo, quiere decir que el
sitio es efectivamente el que queremos visitar y no otro.

Para usarlo es necesario instalar el plugin.

Servidores testigo (notarios)
-----------------------------

Si bien Perspectives ya tiene una lista de servidores a los que
conectarse, nunca viene mal agregar otros más confiables. Mientras más
servidores haya menos posibilidades hay de que un agente malicioso
controle la red y nos engañe a través de Perspectives!

Copiar y pegar esto en las preferencias de Perspectives:

    perspectives.hackcoop.com.ar
    -----BEGIN PUBLIC KEY-----
    MIHKMA0GCSqGSIb3DQEBAQUAA4G4ADCBtAKBrAF3Ge/ghhCsDX4Y0PgWPMhvawGc
    hlb460VvzaEcKKtQDOI4izZvfZokCY9TYzVH0T1tRJICV/FK7jr/Yj2D7BoT8mQk
    KKxZaLSyUUr6Msl3n5ABGc8t/blUSvxYLRdLlEsPjSkYidbf9BI95wwB2ggJhrBL
    nQIj0izhU0zkV5xV3YvMqBBnVsl5USjfBPrettNtGA6B3BVBeGGnYuMRBEKUTtSG
    ODgBKw7jOQ8CAwEAAQ==
    -----END PUBLIC KEY-----

    perspectives.partidopirata.com.ar
    -----BEGIN PUBLIC KEY-----
    MIHKMA0GCSqGSIb3DQEBAQUAA4G4ADCBtAKBrAGcv6MBPTpddKfa+kcKzmFICXg9
    afKanOkwL97ti883YTyyny+toMyzUC+XfeBobNFUTaqB19AAYJ8CBWucgcUNVY9V
    WrMhALelGGrHDfm1evHfe0vwG1LwoCkpPZCM7T1gNYQm68M1f4lyznaU2FG67iqc
    znWyRz2UE9fTDShfAcAkrNdhItr6NVmS5kB+8Bl03AUr3/KtMrupCMY/tzhlu7qc
    tYB0Cyi4mokCAwEAAQ==
    -----END PUBLIC KEY-----

Si lo querés hacer a mano, tenés que visitar el servidor del
[PPAr](https://perspectives.partidopirata.com.ar) y del [HackLab de
Barracas](https://perspectives.hackcoop.com.ar) y copiar el nombre del
servidor y su llave tal como se ve arriba. Si lo querés hacer a mano,
tené
