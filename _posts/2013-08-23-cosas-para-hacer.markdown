---
title: Cosas para hacer
layout: post
categories:
- Media|Medios
---

Cosas que podés hacer para ser pirata
=====================================

Formarte en temas piratas
-------------------------

Como piratas valoramos la autoformación y la discusión informada. Para
eso tenemos que formarnos y estar al tanto de la última información
referida a los temas piratas.

Acá hay una selección de [libros](Libros "wikilink") y
[blogs](Blogs "wikilink") que podés seguir. Compartí los tuyos!

Hablar del Partido Pirata
-------------------------

Nadie se banca a lxs densxs que militan hasta en el baño, pero con
mencionarnos y decir que somos copados vamos pasando la bola y
haciéndonos conocer. Está bueno ser pirata, contémosle a nuestrxs amigxs
P)

Hablar con otros piratas
------------------------

Tenemos distintos medios de comunicación y participación, a saber:

-   [Asamblea
    General](http://asambleas.partidopirata.com.ar/listinfo/general/):
    Nuestra lista de correo principal
-   [Canal de Chat](https://webchat.freenode.net/?channels=ppar):
    Trabajamos y nos divertimos!
-   [Off Topics](Como%20organizar%20un%20encuentro%20pirata "wikilink"):
    Después de trabajar nos juntamos a charlar un rato [lejos del
    teclado](AFK "wikilink").

Hacer cosas a lo pirata
-----------------------

A los piratas nos gusta la [adhocracia](Adhocracia "wikilink"), esto
quiere decir que si vemos una necesidad, tenemos una idea copada para
hacer y que tiene que ver con los principios piratas, no tenemos que
esperar a que lo haga otrx, podemos hacernos cargo y llevarlo a cabo
nosotrxs!

Sólo hay una condición, consultar con lxs demás por si hay desacuerdo e
invitar a otrxs piratas a participar. Los detalles los podés ver en la
sección [Mecanismos de la Democracia
Directa](http://partidopirata.com.ar/documentos/CartaOrganica.html#título-iii-de-los-mecanismos-de-democracia-directa "wikilink")
de nuestra [Carta
Orgánica](http://partidopirata.com.ar/documentos/CartaOrganica.html).

Recordá la canción:

> Si tu tienes muchas ganas de piratear\
> Si tu tienes muchas ganas de piratear\
> Si tu tienes la razón\
> Y no hay oposición\
> No te quedes con las ganas de piratear

Abordar un barco pirata
-----------------------

A veces compartimos intereses con otrxs piratas, para eso están los
barcos piratas! Los barcos son grupos de piratas que se reunen para
trabajar en temas específicos, como el [diario
pirata](http://asambleas.partidopirata.com.ar/listinfo/tiempopirata/),
las [comunas](https://red.anillosur.net/g/ppar) de CABA, las [redes
sociales](https://twitter.com/PartidoPirataAr).

Escribir como pirata
--------------------

Si te gusta escribir o querés practicar, podés enviar notas a nuestro
periódico [Tiempo Pirata](http://tiempopirata.org).

Diseñar como pirata
-------------------

Si te gusta el diseño, podés contribuir a nuestra sección de
[Medios](Categoría:Media "wikilink") con posters, volantes, memes,
diseños para remeras o lo que se te ocurra.
