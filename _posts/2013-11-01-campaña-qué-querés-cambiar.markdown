---
title: Campaña Qué Querés Cambiar
layout: post
categories: []
---

Esta campaña consiste en salir a la calle con el barco callejero y poner
una mesa en alguna esquina transitada de cada comuna para preguntarle a
la gente a través de una encuesta muy sencilla cuáles son los temas que
más le preocupan en la vida cotidiana.

Tendríamos que ir, en principio, por las comunas donde ya hay piratas, y
expandirlo más tarde hacia el resto de las comunas.

\_\_TOC\_\_

Objetivos
=========

1.  Recolectar información acerca de los problemas de una comuna
    particular para luego reinterpretarlos en contraste con los
    principios piratas.
2.  Difundir a través de esta actividad los principio de horizontalidad,
    transparencia y participación colectiva del Partido Pirata.

La encuesta
===========

Como nadie quiere responder encuestas, la idea es hacer algo dinámico
que exprese el espíritu del Partido Pirata. Estoy pensando en una "rueda
de problemas": un panfleto con un círculo dividido en secciones, y cada
sección tendría el nombre de un problema de CABA. Las personas pueden
ponerle un puntaje de 1 a 100 a los problemas que le interesan. Para
comenzar a dar el ejemplo, podemos diseñar esta rueda en una pizarra de
manera que se pueda reutilizar sin desperdiciar papel. Luego de cada
encuesta, los datos se pueden volcar en un dispositivo electrónico para
luego limpiar la pizarra y reutilizarla.

Con la encuesta podríamos sacar las siguientes conclusiones:

1.  Cuáles son las preocupaciones más importantes de cada comuna para
    armar actividades dirigidas a esas comunas.
2.  Contrastar lo que opina la gente de las distintas comunas y armar
    propaganda que reconcilie -en la medida de lo posible- puntos de
    conflicto.
3.  Cómo diseñar un lenguaje relativamente -neutral- para dirigirnos a
    grupos más amplios desde los medios masivos.

¿Cómo empezamos?
================

Para llevar a cabo esta campaña necesitamos:

1.  Un barco callejero por cada comuna (o que el barco callejero vaya
    rotando de lugares).
2.  Diseñar "las mesas" (al menos una por comuna): necesitamos la
    encuesta, una bandera pirata, una mesa, remeras piratas, y un
    discurso o FAQs para ayudar a los barcos callejeros a explicar qué
    es el Partido Pirata en pocas palabras.
3.  Armar un mapa de lugares estratégicos para que los barcos callejeros
    lleven la mesa.
