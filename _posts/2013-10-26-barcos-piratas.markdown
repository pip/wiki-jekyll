---
title: Barcos Piratas
layout: post
categories: []
---

\_\_TOC\_\_

Barcos Piratas
==============

Los piratas nos organizamos en barcos. Cuando tres o más piratas tienen
un interés en común, pueden construir un barco pirata para llevar
adelante el tema. Cualquiera que lo desee puede sumarse a un Barco
Pirata existente. Y por supuesto, ¡no olvidemos el código de
[camaradería
pirata](http://partidopirata.com.ar/documentos/CartaOrganica.html#artículo-5-de-la-camaradería-pirata "wikilink")!

¿Cómo funcionan los barcos?
---------------------------

Los barcos son la base de acción de la comunidad pirata. Cada barco
conquista y conduce un tema particular en beneficio de los demás
piratas. La única condición para esto es respetar el lazo de solidaridad
pirata: estar alineados con nuestros principios. Para que todos los
piratas estemos enterados de nuestras actividades, los barcos deben
informar de sus actividades a la Asamblea Permanente. Por último, el
Partido Pirata es responsable de proveer toda la infraestructura
necesaria para que un barco pueda funcionar: listas de emails, espacio
de almacenamiento, etc.

¿Cómo creo un Barco Pirata?
---------------------------

1.  Juntar tres o más piratas con un interés común.
2.  Presentar el nuevo barco pirata a la Asamblea Permanente para que
    todos puedan opinar y sumarse en el caso que lo deseen.
3.  Pedir que se creen las listas de email y demás infraestructura
    necesaria.
4.  Agregar el nuevo Barco Pirata a esta página.
5.  Arrrr!

¡La Flota Pirata!
-----------------

Estos son los barcos piratas que está activos y cualquiera que quiera
participar puede sumarse a la tripulación.

### [Tiempo Pirata](Tiempo_Pirata "wikilink")

Este barco es nuestro periódico y se encarga de difundir noticias
relacionadas con los intereses piratas.

### [Barco Callejero](Barco_Callejero "wikilink")

Este barco sale a la calle para contarle a todos qué es el Partido
Pirata, difundir nuestros principios y actividades.

### [Barco de Traducción](Barco_de_Traducción "wikilink")

Este barco traduce literatura pirata al castellano, así nadie puede
tener excusa para no leer nuestros libros de inspiración :)

### [Barco de Comunicación](Barco_de_Comunicación "wikilink")

El barco de comunicación se encarga de mantener la presencia y la imagen
pública de los piratas; generar, diseñar y ayudar en la confección de
material de difusión; identificar eventos de interés para que los
piratas puedan asistir.

### [Barco de Infraestructura](Barco_de_Infraestructura "wikilink")

Los hackers que hacen andar nuestras páginas y servidores.
