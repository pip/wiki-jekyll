---
title: Translations:Carta Orgánica/40/en
layout: post
categories: []
---

If the Propposal is approved, either by Consensus or Agreement, the
Pirate who made it in the first place will be responsible before the
pertinent Assembly, either local, general or by ship, and it will have
to work with other Pirates to fulfill it and its agreed period. For
this, the Pirate will conform a work group involving any Pirate willing
to participate.
