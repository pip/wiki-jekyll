---
title: Categoría:Documentos Fundacionales
layout: post
categories: []
---

Documentos fundacionales del Partido Pirata de Argentina, incluyendo
Carta Orgánica, Declaración de Principios y Bases de Acción Política.
