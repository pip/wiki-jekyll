---
title: Translations:Carta Orgánica/6/en
layout: post
categories: []
---

The Pirate Party functions by redefining itself on each of its members
input and by networking with third parties. Its structure is not ex ante
but ex post, during reflection and adoption as its own of the actions of
its members in assembly.
