---
title: Translations:Carta Orgánica/39/es
layout: post
categories: []
---

Si no hubiera Consenso, la propuesta pasará inmediatamente a un período
de discusión y reformulación definido por reglamento, al cabo del cual
pasará a la orden del día para la siguiente Asamblea General.
