---
title: Bandas Piratas
layout: post
categories: []
---

Decile a tus bandas amigas
--------------------------

Contale a tus bandas amigas, a las que conozcas que están pensando en
sacar un disco que pueden hacerlo libre, subirlo a Internet y conseguir
difusión inmediata de la comunidad. Gente que después lo va a ir a ver
si le gusta.

Escribí sobre lo que te gusta
-----------------------------

¿Te gusta una banda que libera sus temas? contanos, escribí algún
articulo en el blog, crea una seccion en la wiki, avisa de eventos, lo
que quieras.

Webs
----

Si conoces comunidades online, blogs, etc sobre bandas copyleft avisa.
