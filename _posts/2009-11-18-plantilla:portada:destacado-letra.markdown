---
title: Plantilla:Portada:Destacado/Letra
layout: post
categories:
- Wikipedia:Plantillas de la portada|{{BASEPAGENAME}}
---

{{\#switch: {{\#expr: ((+)/4 ) mod 4}} \|0=A \|1=B \|2=C
\|3=D}}<noinclude>

A qué letra corresponde el artículo destacado en portada.

-   →

Parámetro opcional: desplazamiento.

-   →

-   →

-   →

-   →

-   →

-   →

Véase también
-------------

-   [Plantilla:Portada:Destacado/A](Plantilla:Portada:Destacado/A "wikilink")
-   [Plantilla:Portada:Destacado/B](Plantilla:Portada:Destacado/B "wikilink")
-   [Plantilla:Portada:Destacado/C](Plantilla:Portada:Destacado/C "wikilink")
-   [Plantilla:Portada:Destacado/D](Plantilla:Portada:Destacado/D "wikilink")

</noinclude><noinclude>
[](Categoría:Wikipedia:Plantillas_de_la_portada "wikilink") </noinclude>
