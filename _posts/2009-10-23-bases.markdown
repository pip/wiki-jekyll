---
title: Bases
layout: post
categories: []
---

Las bases del Manifiesto

Suecia, España, Brasil, etc, básicamente todos tienen algunas ideas en
común, por ejemplo:

Patentes

Derechos de autor

Copyright

Trademarks

Propiedad Intelectual

Universalización de Internet/Inclusión Digital

Transparencia de los gobiernos y su control vía Internet

Monopolios Privados (oposición a los monopolios privados).

Uso de software libre en la administración pública y en la educación
pública.

\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_

Sobre las patentes las dividen en 3 grandes grupos:

Patentes de software.

Patenes de productos farmacéuticos

Patentes sobre la vida, genes/plants.

\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_

Los españoles colocan el tema del Canon Digital (que siempre está
amenazando en la Argentina) en la parte de propiedad intelectual.

Los suecos hablan de \"Liberar nuestra cultura\"

\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_
