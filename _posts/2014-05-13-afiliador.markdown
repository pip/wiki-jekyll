---
title: Afiliador
layout: post
categories: []
---

Problema
========

Necesitamos 6000 adhesiones y afiliaciones para conseguir la personería
jurídica del partido. Las afiliaciones hay que presentarlas por
[cuadruplicado](Como_crear_un_partido_político "wikilink") y sólo hay 90
días de plazo desde la presentación de las adhesiones, sino se pierde el
nombre del partido (y las adhesiones). Todo el trabajo pde ir a buscar
las afiliaciones (por más que la gente lo acepte) implica una logística
casi imposible de ejecutar en ese plazo AFK.

Propuesta
=========

La idea es recibir afiliaciones por internet y después hacer fiestas y
eventos para conocer a la gente, presentarnos y firmar las afiliaciones.

Procedimiento
-------------

-   Las personas se registran como afiliados.
-   Les damos de alta en toda la infraestructura del partido.
-   Se le da la bienvenida en la lista.
-   Se autoasigna un/a pirata sombra.

Threat Model
------------

Se supone que la transmision de los datos es segura ya que utiliza TLS
en modos no vulnerables + Forward Secrecy. Hay que definir mejor la
configuración TLS: definir uso exclusivo de HTTPS con HSTS. Srict
transport security.

Se prevee que un atacante logre obtener acceso remoto o fisico al
servidor donde esten alojados los datos.

Esquema de seguridad
--------------------

1.  Los datos de cada miembro afiliado se guardan en un archivo
    encriptado con una clave pública generada para este fin.
2.  La llave privada está partida en tres partes y cada parte la tiene
    un pirata aleatorio. Se va a utilizar solamente cuando haya que
    llenar las fichas en papel que hay que presentar en la justicia
    electoral, y esto requerirá la presencia física de los tres piratas.
3.  Los miembros pueden desafiliarse con su DNI y una contraseña, que se
    utilizan como nombre de archivo en forma de hash SHA1 (este hash se
    le enviará por email al nuevo afiliado a modo informativo).
4.  La encriptación de los datos y la generación del hash DNI/contraseña
    se hacen en el cliente para evitar que los datos personales lleguen
    al servidor. El servidor sólo recibirá el email y un nombre de
    usuario que **no serán relacionados con el archivo de datos
    personales**, sino que se usará para notificar al partido de la
    existencia del nuevo afiliado para darlo de alta en los servicios, y
    luego se descartarán.
5.  Cuando un miembro se desafilia, los datos encriptados se borran de
    nuestros servidores. Este esquema de seguridad permite que nadie con
    acceso físico o remoto a nuestros servidores pueda leer la
    información de los miembros, por más que lograra descargarse los
    archivos de datos.
6.  Los archivos de datos estarán distribuídos en varios servidores.
