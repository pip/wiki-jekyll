---
title: Proyecto de autonomía alimenticia
layout: post
categories: []
---

El proyecto (según su redacción puede ser una ordenanza o una ley)
establece un cordón alrededor de todas las ciudades y poblados para:

-   Proveer al poblado de alimentos frescos evitando gastos de
    transporte.
-   Proteger a los habitantes de las fumigaciones con agroquímicos
    (ligadas a diferentes tipos de afecciones) .
-   Resguardar la biodiversidad de la zona.
-   Asegurar la disponibilidad de semillas criollas y nativas ante la
    progresiva perdida de estas variedades en favor de variedades
    comerciales privativas.
-   Establecer productores y mercados agroecológicos.

Pad donde estamos trabajando el proyecto:
<http://pad.partidopirata.com.ar/p/LeyDeSoberaniaAlimenticia>

Proyecto de Ordenanza
---------------------

### Antecedentes

#### Historia del mercado de semillas

A principios de la década de 1980 existían en el mundo más de 7000
empresas semilleras, ninguna llegaba al uno por ciento del mercado.
Hacia finales de los \'90 las empresas productoras de agrotóxicos
(Monsanto, Dow, Bayer, Dupont y otras) empiezan a comprar semilleras e
imponer el modelo que se utiliza mayormente hoy en día, la venta de
semillas, agrotóxicos y fertilizantes como un mismo \"paquete\".

Esto llevó a una rápida concentración del mercado de semillas a tal
punto que en 1997 las 10 mayores semilleras controlaban un tercio del
mercado y en el 2005 la mitad. Actualmente solo Monsanto controla el 27%
del mercado total de semillas y más del 80% del transgénico\[0\].

#### Hibridación y selección

Tradicionalmente (y actualmente en muchos circulos) las diferentes
culturas adaptaban sus semillas mediante dos técnicas básicas, la
hibridación, o sea mezclar dos variedades para combinar sus cualidades,
y la selección, seleccionar las semillas de las plantas mas grandes,
resistentes o sabrosas. Las semillas producidas por estos métodos tienen
una gran diversidad genética y muchas veces están adaptadas a las
condiciones de cultivo locales lo que permite continuar el proceso de
hibridación y selección.

#### Los transgénicos

Con el avance del conocimiento genético y la técnica para manipularlo
aparecieron los transgénicos. La técnica consiste en tomar uno o varios
genes de una especie e introducirlo en otra especie para que adquiera
alguna de sus propiedades. La más utilizada es la RR (o RoundUp
Resistant) que la hace resistente al glifosato. Pero existen otras
variedades como las BT, que poduce la toxina Cry, una toxina
insecticida.

Las semillas transgénicas, para optimizar la rentabilidad y obtener una
calidad estandarizada, tienen muy baja diversidad genética, osea todas
las plantas son genéticamente muy parecidas. Ésto hace que la diversidad
y adaptabilidad obtenida a partir de la hibridación y selección sea
mucho menor, a la vez que la actividad es perseguida por las empresas
que reclaman derechos sobre estas semillas.

#### Implicancias para la salud y el medio ambiente

Diferentes estudios han demostrado que la exposición a glifosato y sus
agregados (bajo el nombre de Round Up es el agroquímico más utilizado en
un promedio nacional de 4 litros por habitante por año) produce cáncer,
malformaciones neuronales, cardíacas e intestinales, así como un menor
conteo de esperma y mayores casos de abortos espontáneos y nacimientos
con malformaciones\[1\]\[2\]\[3\].

En nuestro pais se ignora el principio de precaución que dice que ante
una duda justificada deberían tomarse medidas tendientes a limitar el
uso y los efectos del producto y por el contrario se continua aumentando
la superficie fumigada y las cantidades de producto utilizadas por
hectárea.

Mientras tanto se aprueban nuevas variedades de transgénicos para uso
local sin estudios de impacto ambiental y seguridad para el consumo o
solo con estudios de las mismas empresas semilleras que reducen los
plazos y alcances de estos cuando no directamente manipulan los
resultados. Un caso particularmente preocupante es la reciente
aprobación de la soja GM 444Ø6-DAS-6 de Dow AgroSciences Argentina SA
resistente al 2,4-D, un componente del Agente Naranja, y el glufosinato
de amonio.

Asimismo al usar estos agroquímicos no solo se eliminan las plagas, sino
que también mata la flora, fauna y microorganismos encargados de
proteger la tierra de la erosión, polinizar las plantas, prevenir pestes
y mantener la fertilidad de los suelos. El uso sostenido puede llevar a
la contaminación de la tierra y el agua con estos productos, la erosión
por el viento y la lluvia al no tener una cubierta vegetal que lo
proteja y finalmente la desertificación acabando con la capacidad
productiva de esa tierra.

#### Biopatentes y la soberanía alimenticia

Las biopatentes y el intento de imponerlas en el país, ponen la
producción de alimentos en manos de grandes transnacionales, aumentando
los costos de producción a la vez que retira grandes cantidades de
dinero del mercado local en forma de regalías, reduce la diversidad
genética y la producción de variedades nativas. Del total de patentes
registradas cada año solo el %0,5 pertenecen a América Latina y el
Caribe.

#### Autonomía alimenticia

La producción local de alimentos permite que las ciudades y pueblos se
provean a ellos mismos de alimentos de forma más económica y sostenible,
reduciendo los costos de transporte ya que se producen donde serán
consumidas y por medio de los mercados agroecológicos acercando a los
productores y consumidores a la vez que incentiva la actividad comercial
local.

#### Referencias

\[0\] ETC Group Communiqué, September/October 2005
<http://www.etcgroup.org/sites/www.etcgroup.org/files/publication/47/01/commseedspafin.pdf>
\[1\] ADVERSE IMPACTS OF TRANSGENIC CROPS/FOODS A COMPILATION OF
SCIENTIFIC REFERENCES WITH ABSTRACTS
<http://www.indiaenvironmentportal.org.in/files/file/Scientific_Papers_Compiled_March_2013_coalition-for-a-gm-free-india.pdf>
\[2\] Roundup and birth defects - Is the public being keptin the dark?
<http://earthopensource.org/files/pdfs/Roundup-and-birth-defects/RoundupandBirthDefectsv5.pdf>
\[3\] Herbicidas a base de Glifosato produce efectos teratogénicos en
vertebrados interfiriendo en el metabolismo del Ácido Retinoico
Paganelli et al
<https://docs.google.com/file/d/0B7nlZh7hSXvhNTQyNTIwNzAtODQ2NC00ZTU3LThkZDMtYzFlMDUzMjA5MGI1/edit?hl=en_US>

### Proyecto de Ordenanza para la Soberanía y Autonomía Alimenticia

#### Artículo 1 - Fumigaciones aéreas

Se prohíben las fumigaciones aéreas con agroquímicos.

#### Artículo 2 - Fumigaciones terrestres

Se prohíben las fumigaciones terrestres con agroquímicos en un cordón de
dos mil quinientos (2500) metros de cualquier espejo de agua.

#### Artículo 3 - Cinturon agroecológico

Se destinan para la conservación de la biodiversidad y la producción
agroecológica, se prohíben las fumigaciones terrestres con agroquímicos
y el uso de semillas transgénicas en un cinturon alrededor de todas las
ciudades, pueblos y comunidades rurales de cien (100) metros por cada
quinientos (500) habitantes de acuerdo al último censo. En ningun caso
este cinturon podrá ser menor a un mil (1000) metros o mayor a veinte
mil (20000) metros.

#### Artículo 4 - Venta y uso de agroquímicos

a\) Se llevará un registro de vendedores de agroquímicos. b) Cada
vendedor deberá llevar un registro de quien compro agroquimicos, cuales,
que cantidades y donde planea utilizarlos. c) Estos registros se haran
públicos al inicio de cada año fiscal. d) Los compradores deberán
disponer de los envases de forma segura. e) Se limitara el uso de los
agroquimicos a 3 lts por hectarea.

#### Artículo 5 - Penas a los infractores

Se impondrán multas tanto al productor como al fumigador que violen los
artículos 1, 2, 3 y 4 de la presente ordenanza. a) El monto de las
multas se destinaran a un fondo de promoción de las semillas criollas y
nativas. b) El monto de la multa se establecerá al principio de cada año
fiscal, actualizandose cada tres (3) meses, igual a cinco (5) quintales
por hectárea. c) El monto se duplicará por cada reincidencia.

#### Artículo 6 - Promoción de semillas criollas y nativas

Se promoverán festivales, ferias, encuentros, cooperativas y cualquier
tipo de actividad o grupo avocado a la identificación, intercambio,
multiplicación y uso de las semillas criollas y nativas libres de
patentes.

#### Artículo 7 - Promoción de metodos agroecológicos

Régimen de promoción de actividades de producción agroecológica, vegetal
o animal que respeten la biodiversidad y sin impacto negativo sobre el
ambiente, a las que se eximirá por 10 años de tributos comerciales
municipales y del Impuesto Inmobiliario Municipal. a) El registro de
emprendimientos promocionados se publicará al inicio de cada año fiscal.

#### Artículo 8 - Órgano de aplicación

El ejecutivo definirá un organismo de aplicación encargado de recibir
denuncias, llevar los registros y publicarlos, difundir la presente
ordenanza y aplicarla. a) Cualquier ciudadano podrá denunciar la
violacion de la presente ordenanza en cualquier destacamente policial,
sin prejuicio de cualquier organismo definido por el ejecutivo.

#### Artículo 9 - Promoción de mercados agroecológicos

El gobierno municipal dispondrá de un terreno o inmueble para el
desarrollo de un mercado de productos agroecológicos gestionado por sus
participantes.

#### Artículo 10 - Definiciones

A todos los efectos legales se entenderá por agroquímicos a las
sustancias naturales o sintéticas de uso agrícola, de acción química y/o
biológica, que tienden a erradicar especies vegetales o animales de los
cultivos, como también aquellas sustancias susceptibles de incrementar
la producción vegetal y los que por extensión se utilicen en saneamiento
ambiental.

Se deja constancia que quedan equiparados y/o comprendidos en la
definición de agroquímicos los siguientes términos: \"plaguicidas\",
\"herbicidas\", \"funguicidas\", \"acaricidas\", \"insecticidas\",
\"biocidas\" y/o \"productos fitosanitarios\".

A todos los efectos legales se entenderá por semillas transgénicas o
geneticamente modificadas a aquellas que hayan recibido de forma
artificial genes de otras especies o variedades.

A todos los efectos legales se entenderá por producción, productos o
alimentos agroecológicos vegetales aquellos que en su producción no
hicieran uso de agroquímicos ni semillas transgénicas; y producción,
productos o alimentos agroecológicos animales aquellos que en su
producción no usaran antibioticos de forma preventiva ni agregados
hormonales o métodos intensivos como el feedlot o engorde a corral.

#### Artículo 11 - Difusión de la ordenanza

La presente ordenanza, su importancia, así como \"donde\" y \"como\"
denunciar su inclumplimiento, deberán ser difundidos en los medios
locales escritos y audiovisuales.

#### Artículo 12 - Invitación a adherir

Se invita a las provincias, a la Ciudad Autónoma de Buenos Aires y a los
municipios a adherir a la presente ordenanza.
