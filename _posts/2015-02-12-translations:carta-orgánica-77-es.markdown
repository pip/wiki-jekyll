---
title: Translations:Carta Orgánica/77/es
layout: post
categories: []
---

El moderador podrá elegir entre los participantes responsables de llevar
el Acta y de asistirlo en sus tareas.
