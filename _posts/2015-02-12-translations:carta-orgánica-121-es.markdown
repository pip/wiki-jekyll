---
title: Translations:Carta Orgánica/121/es
layout: post
categories: []
---

Los candidatos podrán figurar en las listas con el nombre con el cual
son conocidos, siempre que no dé lugar a confusión y cuando sea vehículo
de identificación inequívoca.
