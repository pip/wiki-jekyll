---
title: Consenso
layout: post
categories: []
---

Las piratas decimos que [hay consenso cuando no hay disenso
relevante](http://partidopirata.com.ar/documentos/CartaOrganica.html#art%C3%ADculo-9-consenso-o-disenso-relevante),
osea no hay piratas en desacuerdo, no han justificado su desacuerdo o no
son suficientes para detener la acción. El porcentaje de disenso
relevante se define en la Reglamentación pero nunca puede ser mayor al
10%. Ahora es 3%.
