---
title: Translations:Carta Orgánica/120/es
layout: post
categories: []
---

Las listas partidarias presentarán, juntamente con el pedido de
oficialización de listas, datos de filiación completos de sus candidatos
y constancia del último domicilio electoral.
