---
title: Entrevista Via Libre
layout: post
categories: []
---

A continuación compartimos una entrevista requerida por Via Libre sobre
la articulación del Partido Pirata y la cultura libre.

------------------------------------------------------------------------

Estimados,

Desde Fundación Vía Libre estamos realizando un pequeño relevamiento de
organizaciones que trabajan en el mundo de la cultura libre y los bienes
comunes intangibles. La razón es la realización de un trabajo
monográfico sobre el tema, que debemos entregar a la brevedad. Les
agradecemos si están dispuestos a colaborar respondiendo estas preguntas
que les enviamos. Para que nos sean de utilidad las respuestas
deberíamos tenerlas el jueves 3 por la tarde. Sabemos que son apenas dos
días pero valoramos enormemente su colaboración, aunque sea con
respuestas breves y rápidas.

Les dejamos las preguntas:

**1) ¿Trabaja desde una perspectiva de bienes comunes?**

Sí, hacia adentro del PPAr fomentamos la cultura de bienes comunes,
sobre todo con lo que respecta al contenido digital (software,
publicaciones, libros) que es donde encontramos más consenso. Hacia
afuera, promovemos la cultura de bienes comunes respecto a la tierra y
las redes informáticas físicas como herramienta para generar mayor
cohesión social y abordar los grandes problemas de alimentación,
vivienda y trabajo que aún fortalecen y amplían las desigualdades en
Argentina. No obstante, cabe aclarar que también estamos a favor de la
libre asociación para realizar actividades económicas siempre y cuando
esto no represente un peligro para el desarrollo de la comunidad o una
amenaza para los recursos naturales.

**2) ¿Tienen como objetivo establecer redes con otras organizaciones que
trabajan desde bienes comunes?**

Sí, siempre y cuando los intereses compartidos no entren en
contradicción con los principios del PPAr instituídos en la Carta
Orgánica.

**3) ¿Cómo creen que se pueden mejorar las redes?**

Colaborando con organizaciones de la tierra, hacktivistas, horizontales
y de reforma de derecho autoral y patentes. También sostenemos que
promover el uso de software y redes libres en la educación formal y en
las dependencias del Estado representaría un cambio de actitud
fundamental para reducir la brecha digital (mejorar el acceso a las
redes y, en consecuencia, la propia infraestructura y cultura digital) y
fomentar entornos de trabajo colaborativos, lo que es esencial para
debilitar la brecha cultural real que existe en nuestro país.

**4) ¿Qué nuevas prácticas sociales creen que es necesario desarrollar
como alternativa al sistema de valores actual?**

-   Desarrollo de redes alternativas y libres para agilizar la
    comunicación y la difusión de las luchas.
-   Difundir y ejercitar las formas de organización horizontales. La
    cooperación e intercambio de conocimiento entre organizaciones
    abocadas a diferentes problemáticas.
-   Institucionalizar el uso de software libre en la educación y el
    Estado.
-   Otorgar financiamiento real a empresas que con sus operaciones,
    además de los beneficios económicos, busquen generar espacios
    autosustentables.
-   Brindar capacitación y asesoría gratuita para las personas o grupos
    que deseen comenzar emprendimientos con las características
    mencionadas en el punto anterior.

**5) ¿Están participando en la construcción de la agenda política de su
región?**

Parcialmente, en este momento estamos viendo cómo nuestros principios se
pueden extender a una agenda más general y rescatar las diferentes
luchas que se están dando en el seno de la sociedad. Entre algunos temas
de la agenda política sobre los que ya llegamos a un consenso podemos
mencionar la explotación de combustibles mediante la técnica de
fracking, la ley de neutralidad de internet y la ley de semillas.

**6) ¿Cuáles son los principales obstáculos en la construcción de esta
agenda?**

-   El lobby sojero y la educacion estatal vinculada a corporaciones.
-   La desinformación de los medios de comunicación masivos.
-   La falta de participación, interés y conocimiento de grandes
    sectores de la sociedad.
-   La falta de financiamiento para una dedicación exclusiva a
    cuestiones del partido.

**7) ¿Cómo se sustenta la organización? cuáles son los principales
desafíos a la hora de sostener económicamente su organización? considera
que necesita ayuda en la construcción de capacidades para sostener
económicamente sus actividades?**

Los miembros del partido se hacen cargo de los gastos que puedan surgir
en un momento dado en la medida en que pueden. No disponemos de ningun
tipo de fondos ni financiación. Tampoco incurrimos en muchos gastos ya
que los principales insumos (servidores y volantes) son donaciones.
Fondos para una sede no estarian de más :)
