---
title: Medios para difusión
layout: post
categories: []
---

### Agencias de noticias

-   <http://www.realpolitik.com.ar/>
-   <http://www.dyn.com.ar/>
-   <http://www.telam.com.ar/>
-   <http://about.reuters.com/latam/showcase/argentina.htm>
-   <http://www.noticiasargentinas.com/>
-   <http://www.totalnews.com.ar/>
-   <http://www.infosic.com.ar/>
-   <http://www.iarnoticias.com/>
-   <http://argentina.indymedia.org/>
-   <http://www.fodema.com.ar/>
-   <http://www.lafogata.org/>
-   <http://www.anred.org/>

### Diarios

-   <http://www.pagina12.com.ar/>
-   <http://www.lanacion.com.ar/>
-   <http://www.clarin.com/>
-   <http://www.infobae.com/>
-   <http://www.perfil.com/>
-   <http://www.momarandu.com>

### Webs

-   <http://www.torrentfreak.com>

### Partidos Piratas

-   <http://www.partido-pirata.com.ar>
-   <http://www.partidopirata.es/blog/>
-   <http://www.partidopirata.cl>
-   <http://www.pp-international.net> Organización coordinadora del
    movimiento internacional de las Partidos Piratas
