---
title: Translations:Carta Orgánica/108/es
layout: post
categories: []
---

-   Su representado así lo designase;
-   Estuviese imputado en el mismo hecho que su representado;
-   Por determinación asamblearia, siempre que existan razones
    valederas;
-   Por autorrevocación, esto es, renuncia al cargo;
-   Por motivos particulares, médicos u otros, no pudiera cumplir con
    sus obligaciones.
-   Si, por cualquier motivo expreso en este Documento, dejase de ser un
    Pirata Afiliado/a.
-   Si fuese imputado en una causa penal/civil/comercial y/o de
    cualquier otro fuero, en los alcances de la Justicia de esta Ciudad.
