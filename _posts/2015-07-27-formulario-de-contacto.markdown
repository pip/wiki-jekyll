---
title: Formulario de contacto
layout: post
categories: []
---

### Manejo mensajes de contacto

El formulario de contacto del blog ofrece una forma de comunicación con
el partido que permite cierta privacidad para quien escribe el mensaje.
Esto no significa que no haya que manejarlo con transparencia.

Para evitar errores de comunicación y malos entendidos con el tiempo
hemos depurado un proceso simple y útil que es el siguiente:

-   Tres piratas reciben los mensajes del formulario de contacto.

<!-- -->

-   Muchas veces los mensajes son consultas genéricas sobre como
    participar o para saber mas sobre el partido, eso se contesta con
    una breve introducción a los objetivos del partido e información
    sobre como participar y unirse a la lista de correos. (No responder
    simplemente con un link)

<!-- -->

-   Las respuestas se envían con copia a los otros dos piratas para que
    estos sepan que se respondió el mensaje y no vuelvan a responder.

<!-- -->

-   En caso de que haya que tomar decisiones o sea información no
    trivial se enviá a la lista de correos para debatirlo o difundirlo
    cuidando de no reenviar información privada de quien escribe. Tener
    en cuenta que es un canal de comunicación privado, consultar antes
    de reenviar de ser necesario.

<!-- -->

-   En caso de entrevistas darle prioridad, recoger la información
    necesaria y buscar quien puede responder a la entrevista. En caso de
    que haya que redactar una respuesta levantar un pad y agitar!!!

<!-- -->

-   Cada algún tiempo los piratas encargados de la tarea cambian.
    También pueden cambiar en caso de que alguien no tenga
    disponibilidad o ya no quiera hacerse cargo de la tarea.
