---
title: Translations:Carta Orgánica/172/es
layout: post
categories: []
---

Cada Pirata Afiliado podrá solicitar un adelanto de presupuesto durante
y para la realización de una Propuesta debiendo presentar los documentos
fiscales a cuenta del Partido que avalen dicho adelanto, así como la
rendición y devolución del excedente del adelanto.
