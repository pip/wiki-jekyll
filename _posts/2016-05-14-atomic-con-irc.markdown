---
title: Atomic con IRC
layout: post
categories: []
---

En el IRC pirata nos juntamos a conversar en tiempo real, intercambiar
información, pelotear ideas\... es como nuestro grupo de Whatsapp pero
en un servidor seguro y pirata P)

Instalación
-----------

[Atomic en
F-Droid](https://f-droid.org/repository/browse/?fdid=indrora.atomic)

[Sitio Oficial](http://papertape.zaibatsutel.net/Atomic/)

Configuración
-------------
