---
title: Translations:Carta Orgánica/40/es
layout: post
categories: []
---

Si hubiera aprobación, tanto por Consenso como por Acuerdo, quien
inicialmente hiciere la propuesta será responsable ante la asamblea
pertinente, sea local, general o de barco y deberá trabajar con otros
piratas para llevarla a cabo en los plazos pautados. Conformará para
ello un equipo de trabajo involucrando a todos los piratas que deseen
participar.
