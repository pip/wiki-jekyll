---
title: Preguntas mas frecuentes
layout: post
categories: []
---

*\' ¿Porque piratas?*\' El nombre es un desafío directo a quienes
quisieron estigmatizarnos con la palabra piratas. Nos llamaron piratas
por querer compartir y negarnos a los monopolios del copyright. Pero
nosotros no lo sentimos como una estigmatización, Creemos que varios de
los valores de antiguos piratas son positivos, como los que nombramos en
nuestro manifiesto pirata y nos aferramos a ellos para unirnos en la
lucha contra quienes quieren avasallar nuestros derechos como seres
humanos a la información, a la educacion, al poder compartir con
nuestros amigos las cosas que nos gustan.

**¿Esto es realmente un partido político?**

Todavía no. pero esta en nuestras metas lograrlo.

**¿Están avalados por el partido pirata internacional?**

El partido pirata internacional nos reconoce como partido pirata en
formación

**¿Cómo puedo afiliarme?**

Como todavia no somos un partido politico propiamente dicho todavia no
tenemos afiliados, Pero si queremos juntar firmas para poder
consolidarnos como tal.
