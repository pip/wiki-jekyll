---
title: Como organizar un encuentro pirata
layout: post
categories:
- Reuniones
- Guías
---

[Categoría:Reuniones](Categoría:Reuniones "wikilink")
[Categoría:Guías](Categoría:Guías "wikilink")

Cómo organizar un Off Topic Pirata
==================================

Los [OT](Off%20Topic%20-%20Fuera%20de%20tema "wikilink") Piratas son
reuniones
[AFK](Away%20From%20Keyboard%20-%20Fuera%20del%20teclado "wikilink") sin
otro fin que juntarse a tomar algo, conocerse y charlar un rato.

Esta es una guía de cómo organizar uno, con consejos sobre qué hacer y
qué no hacer para que salgan más o menos bien P)

Elegir un lugar
---------------

Que sea más o menos barato y accesible al transporte.

Poner una hora
--------------

Más o menos cuando todos estén desocupados, saliendo del trabajo
idealmente. Recordar que si hay que ir a casa y volver a salir a la
mayoría le da fiaca, a partir de las 18hs. por ejemplo.

Ponerse en contacto con los asistentes
--------------------------------------

-   Anunciar el lugar y hora una vez seleccionados, con un tiempo
    prudencial de changüí para que todos se enteren.
-   Pedir que los que vayan a asistir confirmen, adelantando una hora de
    llegada y una forma de contactarlos si hay cambios de planes de
    último momento.
-   Arreglar los detalles en privado pero tratar de minimizar el tiempo
    y la cantidad de mails que toma organizar algo, es decir, ser
    ejecutivo y tener una propuesta firme antes de arrancar.
-   Si a alguien no le gusta el lugar, hora, etc. planificado invitarlo
    cordialmente a organizar el siguiente. Cambiar los planes a último
    momento genera confusión y desgano.
-   Corolario del anterior: traten de elegir lugares que les queden más
    o menos cómodos a todos.

El OT
-----

-   El OT no tiene horarios ni temas específicos!

Consejos
--------

-   Elegir siempre un lugar público, nunca la casa de alguien porque
    nunca se sabe quién puede caer (servicios, pesados, etc.)
-   Si la lista de asistentes es o va a ser pública, agregar algunos
    piratas de más para \"ofuscar\" el evento frente a ojos indeseables.
    Como el OT sólo tiene sentido para los que asisten, no importa
    constatar otra cosa.
