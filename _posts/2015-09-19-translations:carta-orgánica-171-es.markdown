---
title: Translations:Carta Orgánica/171/es
layout: post
categories: []
---

Cada Pirata Afiliado percibirá el reintegro de los gastos generados por
y durante la realización de una Propuesta contra presentación de documen
tos fiscales a cuenta del Partido que avalen dicho reintegro.
