---
title: Translations:Carta Orgánica/80/en
layout: post
categories: []
---

Any responsibility delegated to Affiliated Pirates will have a revocable
and rotatory character.
