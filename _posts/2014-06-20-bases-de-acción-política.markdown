---
title: Bases de Acción Política
layout: post
categories:
- Documentos_Fundacionales
---

-   Fomento y desarrollo de políticas participativas, horizontales,
    democráticas y transparentes en todos los ámbitos, desde las
    organizaciones de base hasta el Estado.

<!-- -->

-   Implementación de políticas ambientales y en defensa de los recursos
    naturales. Reemplazo de tecnologías extractivas y/o productivas que
    dañen el medio ambiente (fracking en petróleo, minería a cielo
    abierto, uso de transgénicos y glifosato, monocultivo sojero, etc.).
    Producción sostenible y enfocada a la satisfaccion de las
    necesidades locales.

<!-- -->

-   Fuerte impulso al desarrollo de las redes neutrales de comunicación.
    Una red neutral (principalmente en internet) es aquella que está
    libre de restricciones en las clases de equipamiento que pueden ser
    usadas y los modos de comunicación permitidos, que no restringe el
    contenido y no admite la censura de sitios y plataformas y donde la
    comunicación no está irrazonablemente degradada por otras
    comunicaciones. Asimismo, fomento de las redes libres, organizadas
    desde las bases.

<!-- -->

-   Fomento y desarrollo de formas alternativas y ecológicas de
    producción de energía para lograr la autosuficiencia energética, con
    el objetivo de reducir al mínimo posible el uso de combustibles
    fósiles, energía atómica y otras fuentes energéticas contaminantes o
    cuya producción degrade el medio ambiente.

<!-- -->

-   Implementación de políticas de uso exclusivo de Software Libre en
    todos los ámbitos del Estado, con principal énfasis en la educación.
    Implementación de libre acceso, distribución y modificación para
    todo el material cultural, científico, estadístico, etc., producido
    o financiado por el Estado.

<!-- -->

-   Reforma de las leyes de derecho de autor y patentes para que el
    libre acceso y la distribucion del conocimiento no sean penados.
    Reducción de la duración del monopolio privado sobre obras e
    invenciones.

<!-- -->

-   Fomento de acciones que aseguren el mejoramiento de los sistemas de
    salud, educación y otros servicios públicos en todos sus niveles
    garantizando el acceso a todas las personas en forma libre y
    gratuita.

<!-- -->

-   Fomento de acciones que faciliten e impulsen la creación de
    proyectos sociales colaborativos, para atacar problemas sociales
    desde las bases.

<!-- -->

-   Fomento de acciones tendientes a construir una sociedad en que la
    propiedad de los medios de producción de bienes tanto tangibles como
    intangibles sea colectiva o social, en contraposición al actual
    régimen capitalista de la propiedad privada.

<!-- -->

-   Impulso de leyes que protejan y amplíen el derecho a la privacidad
    de todas las personas. Eliminación de todos los registros de datos
    innecesarios para garantizar el derecho al anonimato y el derecho a
    administrar sus datos personales a todas las personas.

<!-- -->

-   Fomentar la prohibición de todo tipo de vigilancia. Asegurar la
    inviolabilidad de cualquier tipo de comunicación (oral, postal,
    electrónica, etc.) por parte del Estado y de privados. Eliminación
    de los sistemas de recolección de datos biométricos y cualquier tipo
    de sistema de vigilancia masiva.

<!-- -->

-   Impulso de leyes de restitución de tierras y reconocimiento
    histórico a los pueblos originarios.

[Categoría:Documentos\_Fundacionales](Categoría:Documentos_Fundacionales "wikilink")
