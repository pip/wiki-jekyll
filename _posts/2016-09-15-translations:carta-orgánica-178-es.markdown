---
title: Translations:Carta Orgánica/178/es
layout: post
categories: []
---

Los piratas que causen malestar o desorganización, incumplan
repetidamente tareas delegadas o auto-asumidas, o no respeten los
principios piratas podrán ser expulsados mediante Propuesta.
