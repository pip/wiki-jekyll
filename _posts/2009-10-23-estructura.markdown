---
title: Estructura
layout: post
categories: []
---

BORRADOR MANIFIESTO PARTIDO PIRATA ARGENTINA

-ORDEN TEMAS

-Introducción -Sobre la Libre Cultura -Sobre la Libertad de Información
y Expresión -Sobre los \"Aprietes\" en Argentina a la Libertad de
Información y Expresión -Copyrights / Derechos de Autor -Patentes
-Trademarks -Sobre el Estado Argentino ( Estado Trasnparente ) -Sobre
Derechos del Individuo ( defensa de la privacidad, etc. ) -Sobre otros
temas fuera de este manifiesto y nuestra política general.

-MANIFIESTO ( borrador del borrador)

Intro: Un fenómeno masivo, fenómeno que nadie hace nada por controlar, y
la disposición penal no sirve para nada. (Zafaroni)

DDHH Fundamentales\...

-   libertad de expresión,

`*privacidad,`\
`  *presunción de inocencia y derecho a un juicio justo,`\
`  *igualdad ante la ley y no discriminación,`\
`  *y derecho a la vida y a la integridad física y moral.`

Velar y asegurar la libertad de expresión y la libertad de cultura, sin
tapujos ni censura alguna.

Copyrights: Reformar los Copyrights; legalizar el intercambio de
archivos y reducir la excesiva protección de la ley de copyright,
asegurando que cuando una obra creativa sea vendida, el beneficiado sea
el Artista y no los poseedores de las leyes de monopolio. ( español)

Marcas Registradas: El abuso de las marcas registradas, como sustituto
de o añadido a los derechos de autor, es algo que no se puede permitir
que continue, dado que mina la confianza del público. Acorde con ésto,
el material sujeto a derechos de autor no podrá ser susceptible de
convertirse en marca registrada, y del mismo modo las marcas registradas
no podrán quedar sujetas a derechos de autor. ( español)

Derechos del Individuo: Terminar con la excesiva vigilancia, monitoreo y
seguimiento de personas inocentes tanto por el gobierno como por los
grande grupos económicos.

-COMENTARIOS AL PIE DE PAGINA / PUNTOS CONFLICTIVOS\...
