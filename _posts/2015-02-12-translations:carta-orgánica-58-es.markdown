---
title: Translations:Carta Orgánica/58/es
layout: post
categories: []
---

Las Asambleas son capaces de mandatar responsables voluntarios
individuales o grupales para cumplir tareas necesarias para el
funcionamiento regular del Partido. La delegación de responsables se
hará mediante Propuesta.
