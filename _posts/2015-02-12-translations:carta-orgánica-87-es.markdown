---
title: Translations:Carta Orgánica/87/es
layout: post
categories: []
---

Todas las posiciones en los Órganos Burocráticos del Partido son
delegadas y revocadas por decisión asamblearia.
