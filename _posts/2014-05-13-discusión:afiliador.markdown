---
title: Discusión:Afiliador
layout: post
categories: []
---

1.  Cómo participar en este pad

Ponete un nick (arriba a la derecha)

-   No borres los colores sin consensuar antes (en el chat)
-   No borres el texto, usá el botón de tachado y escribí alrededor
-   Si vas a hacer un comentario hacelo así:

`   // muy bueno! -- fauno`

====

1.  Problema

Necesitamos 6000 adhesiones y afiliaciones para conseguir la personería
jurídica del partido. Las afiliaciones hay que presentarlas por
cuadruplicado \[1\] y sólo hay 90 días de plazo desde la presentación de
las adhesiones, sino se pierde el nombre del partido (y las adhesiones).
Todo el trabajo pde ir a buscar las afiliaciones (por más que la gente
lo acepte) implica una logística casi imposible de ejecutar en ese plazo
AFK.

Hay que tener en cuenta que esos datos se publican una vez que se
presentan en la junta electoral (pero hay que confirmar donde, como y si
son todos los datos)

1.  Propuesta

La idea es recibir afiliaciones por internet y después hacer fiestas y
eventos para conocer a la gente, presentarnos y firmar las afiliaciones.

1.  1.  Procedimiento

-   Las personas se registran como afiliados.

<!-- -->

-   Les damos de alta en toda la infraestructura del partido.

<!-- -->

-   Se le da la bienvenida en la lista.

<!-- -->

-   Se autoasigna un/a pirata sombra.

Prototipo: <http://spiria.me:7007/> Código fuente:
<https://github.com/piratas-ar/pirate-crew>

1.  1.  Threat Model

Se supone que la transmision de los datos es segura ya que utiliza TLS
en modos no vulnerables + Forward Secrecy. //definir mejor la
configuración TLS. Definir uso exclusivo de HTTPS con HSTS. Srict
transport security \--oeron

Se prevee que un atacante logre obtener acceso remoto o fisico al
servidor donde esten alojados los datos. //una vez comprometido el
atacante puede optener los datos antes de que se encripten \--oeron //
chroot all the things!! (?) \--nicoff

1.  1.  Esquema de seguridad

1\. Los datos de cada miembro afiliado se guardan en un archivo
encriptado con una clave pública generada en el momento para este fin.

2\. La llave privada se encripta con la llave general pirata y con la
llave generada para la pirata. \-- Demasiado complejo para mi gusto,
abajo se propone algo similar pero simplificado \--seykron

3\. La llave privada general la tienen tres piratas aleatorios, y se va a
utilizar solamente cuando haya que llenar las fichas en papel que hay
que presentar en la justicia electoral. //definir un esquema para que x
de N personas sean necesarias , 2 de 3 por ejemplo . Dividir la pa
ssphrase o clave secreta en N partes. Por ejemplo usando Shamir secret
sharing. // Se lo plantie a seykron. La conclusion que llegamos es usar
PK no por las caracteristicas copadas de la PK, sino porque si usasemos
passwords, deberiamos almacenarlo en texto plano en el server. \--nicoff
// Tener un par de PK\'s permite incriptar con la Public y tener la
private en otro lado. \--nicoff // Si alguien que no soy yo se encarga
de hacerlo y mantenerlo es bienvenido, eh :P \--seykron //Entiendo el
uso de una PubKey, esta perfecto es un \"write only\" pero la PrivKey se
puede dividir en N partes para que no haya un posible leaker y deban ser
más de uno \--oeron // como las llaves dobles para autodestruir el
mundo!! \-- fauno //exacto \--oeron // Se lo plantie eso a seykron. Me
preocupa mas el hecho de hacer reissue de todas las claves en caso de
que una este comprometida. Lo de Shamir Secret Sharing me parece
overkill \--nicoff // Yo no quiero laburar sobre eso, si alguien lo hace
sería ideal \--seykron // le mandas un cacho de pem a cada pirata y para
descifrar hay que concatenarlo e importar en un llavero \-- fauno //MUY
FACIL: la passphrase con la que se encripta la PrivKey que sea larga y 3
persona conocen un pedacito de esta, lo tipean al crearla y al recuperar
los datos. Esas personas pueden informarle su pedacito a otras por si se
mueren o se olvidan. ESto no soluciona que 2 de 3 puedan hacerlo, es un
3 de 3. \--oeron // BRILLANTE :) // Huy me olvide mi pedazo! Chau data
de afiliados :( \--nicoff // Caminar por la plancha!!! \--seykron // lo
anotas, se lo das a alguien de confianza \--oeron // distribucion +
replicacion 3x3 :P //En lugar de dividir en pedacitos la passphrase
puede encriptar como una cebolla, esto permite hacer el protocolo de
desencripcion a distancia si no pueden juntarse fisicamente. A recibe Ma
y devuelve Mb=d(Ka,M) Noasi p es ideal, pero es igual de seguro si se
juntan fisicamente y es un poco mas inseguro a distancia pero permite
hacerlo mientras que del otro modo la parte deberia informar su pedacito
por alun medio y asi perder el secreto. \--oeron // me cabe \-- fauno //
ahora si alguien tiene que revocar la llave estamos cagados \-- fauno
//no deberia ser necesario porque lo que se puede comprometer es la
publica, que es publica, la pueden cambiar por otra. La privada debe
estar segura ..encriptada con la passphrase en pedacitos \--oeron //Me
parece que antes que meternos en mas quilombo con esto, hay que ver el
tema de la infra y como mitigar que nos cojan las afiliaciones a traves
del wordpress //Las dos cosas son igual de importanes. el \"no meternos
en tanto quilombo\"es el principio de todas las catastrofes de
seguridad. La seguridad es un quilombo, si no te gusta no lo hagas.
\--oeron //Para mediocridad ya tenemos suficientes partidos
politicos.Suficiente para mi. Adios \--oeron // ¿Entonces cuál es el
punto medio de esto? Tal vez con dividir el PEM de la key privada y
borrarla físicamente alcanza para lo que necesitamos, porque todos //
los que participamos nos encontramos a menudo. \--seykron

3\. Los miembros pueden desafiliarse con un código de afiliado
autogenerado (o la llave privada de la pirata) y una contraseña, que se
utilizan como nombre de archivo en forma de hash SHA1. //SHA1 +
contraseña es demasiado requerimiento, donde lo anotan? se envia por
mail? \--oeron // sí, el id es un uuid generado por el server, la
persona solamente ingresa una contraseña, y el par uuid/pass se manda
por email. \--seykron //no veo la necesidad de enviarle el uuid. EL uuid
puede ser un password-based key derivation function (PBKDF)
f(email+clave). Y generarlo con javascript en el cliente. Requiere que
el usuario confie en //en que el servidor no almacenara el email y
clave, pero es una confianza tolerable. +1 //Si fuera solo f(mail)
podrian borrarse datos solo conociendo los mails, si fuera f(clave)
podria haber muchas coincidencias de claves tontas. El email puede
remplazarse por DNI en caso de que el DNi sea un dato necesario a
guardar y el email y no y se quiera evitar unir dni con email
innecesariamente. El requerimiento de email podria ser opcional, al
menos la union de datos personales con email deberia evitarse. Se
subscribe a la lista pero \"se olvidan\" los datos personales de ese
email. LA encripcion con la clave publica podria hacerse en el cliente
en javascript, esto no evita que modifiquen el javascript luego de
comprometer el servidor pero el javascript modificado puede ser
detectado por robots mientras que guardar los datos sin encriptar en el
server es indetectable. \--oeron // entiendo, esto lo agrego \--seykron

4\. Cuando un miembro se desafilia, los datos encriptados se borran de
nuestros servidores. Este esquema de seguridad permite que nadie con
acceso físico o remoto a nuestros servidores pueda leer la información
de los miembros, por más que lograra descargarse los archivos de datos.

//Distribuir: no hay necesidad de todos los huevos en una canasta,
multiplicar el servidor, repartir los daots guardados, usar más de un un
Certificate Authority, hosting provider. Tener dominios alternativos
preparados y acceso por TOR. \--oeron // En el chat estamos hablando de
usar gluster dentro de librevpn, donde hay nodos que todos conocemos.
\--seykron

\[1\]
<http://wiki.partidopirata.com.ar/Como_crear_un_partido_pol%C3%ADtico>
