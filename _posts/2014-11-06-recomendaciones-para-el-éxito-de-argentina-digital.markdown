---
title: Recomendaciones para el éxito de Argentina Digital
layout: post
categories:
- Noviembre 2014
- Recomendaciones a proyectos de ley
- Argentina Digital
---

[Categoría:Noviembre 2014](Categoría:Noviembre_2014 "wikilink")
[Categoría:Recomendaciones a proyectos de
ley](Categoría:Recomendaciones_a_proyectos_de_ley "wikilink")
[Categoría:Argentina Digital](Categoría:Argentina_Digital "wikilink")

> Publicado en [nuestro
> blog](http://partidopirata.com.ar/2014/11/06/recomendaciones-para-el-exito-de-argentina-digital/)
> el 2014-11-06

El Partido Pirata es una organización internacional abocada
principalmente a la defensa y promoción de la cultura libre, el libre
acceso a la cultura e información y la defensa de la privacidad. Fue
fundado originalmente en Suecia, y desde el 2007 el capítulo argentino
lleva trabajando en temas que competen a la democracia, horizontalidad,
transparencia y derechos humanos, especialmente aquellos relacionados a
las redes de comunicación.

Al enterarnos del proyecto festejamos la decisión de declarar como
servicio e interés público las redes de comunicación y sus recursos
asociados así como la intención de proteger la privacidad de los
ciudadanos y la neutralidad de las redes. Aún así, especialmente
conociendo el trabajo que ha venido haciendo esta comisión durante los
últimos dos años y con la cual hemos colaborado en repetidas ocasiones,
creemos que hay ciertas definiciones y detalles que deberían mejorarse
antes de que este texto se convierta en ley.

Los puntos a mejorar que desarrollaremos son:

-   Definir Neutralidad de la Red
-   Aclarar cuestiones de privacidad e inviolabilidad de las
    comunicaciones
-   Incluir a las Redes comunitarias
-   Acceso a los NAPs
-   Autoridad de Aplicación

Definición. Neutralidad de las Redes.
=====================================

La precisa definición de este concepto es central para garantizar las
libertades expresadas en él, por esto creemos que debe ser definido en
el cuerpo de la presente ley incorporando la definición ya consensuada
por esta misma comisión el 22 de octubre del corriente.[^1]

Privacidad e inviolabilidad de las comunicaciones
=================================================

El Artículo 5 del proyecto establece que "La correspondencia, entendida
como toda comunicación que se efectúe por medio de Tecnologías de la
Información y las Comunicaciones (TIC) autorizadas, entre las que se
incluyen los tradicionales correos postales, el correo electrónico o
cualquier otro mecanismo que induzca al usuario a presumir la privacidad
del mismo, es inviolable". Se deben tener en cuenta las siguientes
consideraciones:

1.  el Art. 6 (Definiciones) no especifica qué son las Tecnologías de la
    Información y las Comunicaciones, sino qué son los Servicios de TIC.
    En este sentido, TIC puede corresponder a cualquier protocolo de
    comunicación, dispositivo, aplicación, etc.
2.  El texto indica que sólo las comunicaciones mediante TICs
    autorizadas son inviolables y no está definido cuáles son estas.
    Cabe la posibilidad de que se interprete como que existen ciertas
    tecnologías, medios o sitios autorizados y por lo tanto otros que no
    lo estarían, volviendo a la distinción entre tráfico "legal" e
    "ilegal" que fuera retirada del dictamen sobre la neutralidad de la
    red. Recomendamos suprimir la palabra "autorizada".

Redes Comunitarias
==================

Las Redes Comunitarias son un elemento fundamental que no puede faltar
en una ley que desee garantizar el derecho humano a las comunicaciones,
ya que permite el acceso a las comunicaciones en regiones muy alejadas
donde a menudo ni el sector público ni el privado pueden llegar a
proveer acceso.

Reconocimiento de las Redes Comunitarias
----------------------------------------

Una Red Comunitaria es aquella construída por la comunidad que hará uso
de la misma con el sólo objeto de contar con una infraestructura de
comunicación comunitaria, autónoma y sustentable. En esta clase de
emprendimientos, el objetivo principal no es obtener una renta por el
uso de la infraestructura, sino mantener y garantizar el correcto
funcionamiento de la misma permitiendo su uso generalizado por parte de
la comunidad.

Cabe destacar que esta clase de emprendimientos es una actividad que ya
se realiza de hecho en función de una necesidad concreta que surgió en
diferentes regiones del país utilizando las bandas no licenciadas. Como
ejemplo podemos mencionar Delta Libre, [Quintana
Libre](http://quintanalibre.org.ar/) y
[Lugro-Mesh](http://www.lugro-mesh.org.ar/), entre muchas otras.

Acceso a los NAPs para las Redes Comunitarias
---------------------------------------------

Hoy en día una de las principales limitaciones que enfrentan las Redes
Comunitarias es la dificultad para conectarse con Internet y formar
parte de la misma, ya sea a través de un Carrier o conectándose a un
NAP[^2] (bajo el control de CABASE). Para garantizar el Servicio
Universal las Redes Comunitarias necesitan un acceso a Internet que hoy
les es inviable debido al requisito de una licencia[^3] y los altos
costos[^4], problemas que serían fácilmente sorteables con una
infrastructura de acceso público.

Promoción de las Redes Comunitarias
-----------------------------------

Debido a que las Redes Comunitarias extienden el acceso a las
comunicaciones en consonancia con el Servicio Universal, y teniendo en
cuenta su bajo costo de creación y mantenimiento (ya que la misma
comunidad se encarga de ello), una ley como Argentina Digital debe
definir la asignación de fondos para estos emprendimientos.

La asignación de fondos permite extender la infraestructura de
telecomunicaciones delegando la gestión a los actores locales, lo que
significaría una reducción de costos operativos para la nación y se le
otorgaría autonomía a las regiones del país con menos conectividad.

Destacamos que actualmente se intenta alcanzar el Servicio Universal
construyendo Nucleos de Acceso al Conocimiento cuando una Red
Comunitaria podría dar acceso a un area mas extensa y construiría una
infrastructura de comunicaciones extensible, resiliente y en manos de
sus interesados. Por lo tanto, consideramos que las Redes Libres pueden
complementar el plan Argentina Conectada.

¿Qué pasa con los puntos neutros de acceso (NAPs)?
==================================================

A través de los NAPs se obtiene el acceso internacional a Internet y la
interconexión entre las redes locales. En Argentina los NAPs están bajo
el control de [CABASE](http://cabase.org.ar), una cámara empresaria de
ISPs.

Si el proyecto de ley considera Internet como un servicio público y
universal, debería controlar los NAPs ya que son la base de éste.

Características de la Autoridad de Aplicación?
==============================================

Dada la extensión de los poderes asignados a la Autoridad de Aplicación
creemos que las siguientes serian cualidades deseables:

-   Que sea autárquica y autónoma.
-   Que tenga un sistema de oposición partidaria, similar al que tiene
    hoy en día el AFSCA.
-   Que incluya académicos.

Referencias
===========

<references/>

[^1]: [Vía Libre - Dictamen favorable a la neutralidad de la red en el
    Senado
    Nacional](http://www.vialibre.org.ar/2014/10/22/dictamen-favorable-a-la-neutralidad-de-la-red-en-el-senado-nacional/)

[^2]: [CABASE - ¿Qué es un
    NAP?](http://www.cabase.org.ar/wordpress/que-es-un-nap/)

[^3]: [Requisitos
    CABASE](http://www.cabase.org.ar/wordpress/requisitos-de-membresia/)

[^4]: [Costo
    CABASE](http://www.cabase.org.ar/wordpress/costo-de-membresia/)
