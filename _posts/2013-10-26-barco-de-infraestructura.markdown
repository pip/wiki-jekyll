---
title: Barco de Infraestructura
layout: post
categories:
- Barco de infraestructura
---

[Categoría:Barco de
infraestructura](Categoría:Barco_de_infraestructura "wikilink")

El Barco de Infraestructura se encarga de las infraestructuras digitales
del Partido Pirata. Mantiene y configura los servidores, etc.

Actividades
-----------

Tripulación
-----------

[Pirata:Fauno](Pirata:Fauno "wikilink")

[Pirata:Godlike](Pirata:Godlike "wikilink")

[Pirata:Mpj](Pirata:Mpj "wikilink")
