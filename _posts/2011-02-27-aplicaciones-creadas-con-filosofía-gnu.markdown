---
title: Aplicaciones creadas con filosofía GNU
layout: post
categories: []
---

Charla sobre software libre, en especial mostrar alguna aplicaciones que
estoy realizando a partir de la \[filosofía
GNU\|<http://www.gnu.org/philosophy/philosophy.es.html>\] basándome en
el criterio de algunas aplicaciones del software libre, por ejemplo
PureData, GNUplot, Mplayer, Emacs, etc, en especial en sus criterios de
personalización, el acceso con terminales o los diagramas de
flujo/canalizaciones; saliendo de la idea convencional de software
comercial de interfaces homogeneizadas, sin dejar de lado la idea de
reversionar o reutilizar el código fuente.

Mi idea seria mostrar como ejemplos varias aplicaciones diferentes
usando:

-   GNU Pichiciego, un navegador web altamente personalizables, simple,
    y ultraliviano.
-   Mashku: un programa para hacer animación (que utiliza como
    interface: tanto terminal, como interfaces no convencionales y
    hardware libre)

y sobre todo mostrar como estos programas se nutren y nutren al GNU,
usando un criterio totalmente al que software restrictivo utiliza.
