---
title: Translations:Carta Orgánica/111/es
layout: post
categories: []
---

Las bancas en ambas cámaras del Congreso ganadas mediante sufragio,
pertenecen al Partido. Se considerará por lo tanto a los representantes
del Partido que ocupen dichas bancas Delegados Piratas que deberán
regirse según el mandato de las Asambleas Piratas.
