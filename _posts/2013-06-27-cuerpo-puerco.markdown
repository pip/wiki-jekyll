---
title: Cuerpo puerco
layout: post
categories: []
---

COLECTIVO

*\' eti.que.tas\_*\'

*\'porno, arte, diseño, vida contemporánea,*

*\' localización*\'

La Plata, Buenos Aires

\'[1](http://www.cuerpopuerco.com.ar/)

*\' Descripción\_*\'

CUERPO PUERCO es producto de un estudio acerca de los vínculos y límites
entre la pornografía, el arte y el diseño en la vida contemporánea.

Lo cotidiano/banal/obseno/privado-industrializado, se materializa y
resignifica en un doble juego (visual-social) de imágenes pornográficas
intervenidas estilísticamente.

Siendo la indumentaria \"Un sistema simbólico vinculado a la
sexualidad\", creemos idónea su funcionalidas como primer soporte para
este desafío.

Los invitamos a sumar a nuestro trabajo distintas miradas\...

[Category:Nodo](Category:Nodo "wikilink")
