---
title: Translations:Carta Orgánica/30/es
layout: post
categories: []
---

El Partido Pirata se compromete a mantener, desarrollar e implementar
medios de comunicación y participación de acuerdo con los principios y
bajo el espíritu de este documento. Es responsabilidad del Partido en su
conjunto fomentar el acceso, conocimiento y capacitación en la
apropiación, utilización y reutilización de estas infraestructuras,
tanto digitales como sociales y políticas.
