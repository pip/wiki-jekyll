---
title: Emprendimientos P2P
layout: post
categories:
- Propuestas_nuevas
- Propuestas_nuevas
---

Descripción
-----------

Se busca la creacion de un Barco de Economía P2P que sirva como punto de
partida de nuevos emprendimientos P2P

Responsabilidades del Barco de Economia P2P
-------------------------------------------

-   Crear una cooperativa que sirva de paraguas para otras cooperativas,
    ahorrando en costos y compartiendo socios.
-   Promocionar cooperativas P2P
-   Promocionar otras modalidades de organizacion P2P
-   Concientizar sobre la necesidad de usar y participar de los nuevos
    emprendimientos
-   Coordinar las actividades de los interesados: nuevas ideas, búsqueda
    de socios, difusión de las cosas que realiza cada cooperativa P2P,
    asesoramiento legal (?)

Tareas a realizar por el proponente
-----------------------------------

[Categoría:Propuestas\_nuevas](Categoría:Propuestas_nuevas "wikilink")

[Categoría:Propuestas\_nuevas](Categoría:Propuestas_nuevas "wikilink")
