---
title: Archivo:La propiedad intelectual es el robo.jpg
layout: post
categories: []
---

[La propiedad intelectual es el
robo](https://secure.flickr.com/photos/christopherdombres/4999371196/in/photostream)
\-- por [Christopher
Drombres](https://secure.flickr.com/photos/christopherdombres/),
[cc-by](http://creativecommons.org/licenses/by/2.0/deed.en)

[Category:Media](Category:Media "wikilink")
