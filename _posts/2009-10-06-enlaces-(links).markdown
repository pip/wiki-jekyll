---
title: Enlaces (Links)
layout: post
categories: []
---

**Encliclopedia Libre:** <http://www.wikipedia.org>

**Filosofía de la Cultura Libre, Derechos de Autos, patentes y Software
Libre:** <http://www.gnu.org/philosophy/philosophy.es.html>

-   -   Pirate Party International (Partido Pirata Internacional) o

PPInternational (PPI), organización coordinadora del movimiento
internacional de las Partidos Piratas\*\*\*
<http://www.pp-international.net>
