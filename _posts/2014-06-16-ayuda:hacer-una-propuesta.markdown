---
title: Ayuda:Hacer una propuesta
layout: post
categories:
- Barco_de_infraestructura|{{PAGENAME}}
- Organización|{{PAGENAME}}
- Propuestas|{{PAGENAME}}
---

Las [propuestas](:Categoría:Propuestas "wikilink") son la forma en que
presentamos ideas y acciones en las asambleas piratas. Se espera que la
persona que hace la propuesta se haga responsable de llevarla a cabo.

Todas las propuestas siguen el proceso de consenso desde que se
presentan hasta que se realizan!

Para hacer una propuesta usando el wiki
---------------------------------------

-   Ir a [Plantilla:Propuesta](Plantilla:Propuesta "wikilink") y copiar
    la plantilla
-   Crear un artículo nuevo y pegar la plantilla
-   Modificar a gusto
-   Enviar al [Barco Pirata](Barcos_Piratas "wikilink") que corresponda!

[](Categoría:Barco_de_infraestructura "wikilink")
[](Categoría:Organización "wikilink")
[](Categoría:Propuestas "wikilink")
