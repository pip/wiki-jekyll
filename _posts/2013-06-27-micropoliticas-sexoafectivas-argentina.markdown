---
title: Micropoliticas sexoafectivas argentina
layout: post
categories: []
---

Proyecto planteado desde el grupo de investigación de la UNLP
(universidad Nacional de La Plata- ) Facultad de Bellas Artes
"Micropolíticas de la Desobediencia sexual en el arte argentino
contemporáneo en el contexto del Laboratorio de Investigación y
Documentación en Prácticas Artísticas Contemporáneas y Modos de Acción
Política en América Latina

Sistematización de prácticas de desobediencia sexual en el arte
argentino en forma de contracartografía de interrelaciones con
pretensiones de convertirse en una plataforma interactiva que recopile a
modo de archivo dinámico-mutante prácticas transfronterizos.

Construir un archivo activo de material (textos, libros, audiovisual,
etc.) generado a partir de las prácticas anteriormente mencionadas, de
un modo similar al proyecto GAN (véase Antecedentes) y por tanto
contagiándose de objetivos como:

explorar el lenguaje científico desde una perspectiva de crítica
feminista. enfatizar las interconexiones con el resto de galaxias,
poniendo de manifiesto el contexto y las narrativas subyacentes al
lenguaje y el conocimiento cientificos. busca trabajar sobre el
potencial transformador de las narrativas subjetivas planteado por
algunas productoras culturales que analizan, ficcionan y cuestionan los
modelos de representación del territorio y de la ciencia.

`generar una psicogeografía compleja que debate los roles y los formatos de la investigación historiográfica de las imágenes.`
