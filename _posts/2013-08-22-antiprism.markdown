---
title: AntiPRISM
layout: post
categories: []
---

![](Antiprism.jpg "Antiprism.jpg")

Estamos horrorizados al conocer el **seguimiento sin precedentes de los
usuarios de internet** a través de PRISM y programas similares.
Vigilancias generalizadas como ésta, especialmente cuando son
implementadas sin conocimiento ni consentimiento de los ciudadanos,
**amenazan seriamente los derechos humanos de libertad de expresión y
privacidad, y con ellos, los cimientos de nuestras democracias**.

Aplaudimos a Edward Snowden, que con sus actos ha destapado estas
prácticas. Cuando un gobierno está realmente por y para la gente, no
puede ser un crimen filtrar información sobre el rumbo y el alcance de
las acciones que realiza el gobierno en nombre de los ciudadanos,
alegando que su propósito es protegerlos. Un gobierno representativo en
democracia depende del consentimiento del pueblo. Sin embargo, tal
consentimiento no puede existir cuando los ciudadanos no han sido
completamente informados.

El próximo sábado 7 de septiembre nos unimos a las actividades Freedom
Not Fear (Libertad, No Miedo) del movimiento pirata internacional con
talleres de privacidad y seguridad en el HackLab de Barracas -
Aristóbulo del Valle 1851 a partir de las 15hs.

Cosas que podés hacer YA!
-------------------------

-   [Consultá la guia de
    privacidad](http://partidopirata.com.ar/2013/04/20/ya-esta-disponible-para-descargar-el-manual-de-la-cryptoparty-en-espanol/)
-   [Pasate a
    GNU/Linux](https://www.gnu.org/distros/free-distros.es.html)
-   Nunca pongas tus datos reales en Internet (usa un pseudónimo)

Veni a aprender:

-   [Como configurar tu
    navegador](http://partidopirata.com.ar/2012/07/20/extensiones-libres-para-mozilla-firefox-y-derivados/)
-   Como encriptar tus conversaciones ([GNU/Linux, Windows,
    etc.](Pidgin_con_OTR "wikilink") o
    [Android](Gibberbot_con_OTR "wikilink"))
-   Como pasarte a GNU/Linux
-   Herramientas para periodistas y ciberactivistas

Este texto es [Dominio
Público](https://creativecommons.org/publicdomain/zero/1.0/).
