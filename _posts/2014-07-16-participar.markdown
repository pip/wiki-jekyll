---
title: Participar
layout: post
categories: []
---

Como Participar
---------------

### Antes que nada [Anotate en tu comuna!!](http://taz.partidopirata.com.ar/afiliate/afiliaciones)

Esto nos va a ayudar a agruparnos en grupos de trabajo por barrio.

### [Como participar del partido](Como_Participar "wikilink")

Como nos organizamos internamente y que herramientas utilizamos para
comunicarnos.

### [Barcos](Barcos_Piratas "wikilink")

Los Barcos son las unidades de accion directa del movimiento, podes
participar.

### [Cosas para hacer](Cosas_para_hacer "wikilink")

¿Que puedo hacer? ¿Cómo puedo ayudar? ¿En que puedo participar? Tambien
revisa las tareas de los [Grupos de
trabajo](Grupos_de_trabajo "wikilink")

### [Reuniones](Reuniones_y_temáticas "wikilink")

Aveces hacemos reuniones físicas, esta al tanto al blog y redes sociales
para enterarte.
