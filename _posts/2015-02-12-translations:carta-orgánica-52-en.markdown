---
title: Translations:Carta Orgánica/52/en
layout: post
categories: []
---

Any group with at least three (3) Affiliated Pirates can ask recognition
as a Pirate Ship by means of a Propposal to the Permanent Assembly.
