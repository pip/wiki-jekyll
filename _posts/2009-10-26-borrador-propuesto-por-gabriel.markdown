---
title: Borrador propuesto por Gabriel
layout: post
categories: []
---

-Intro:

Un fenómeno masivo, fenómeno que nadie hace nada por controlar, y la
disposición penal no sirve para nada. (Zafaroni) Quema de Libros por
parte de Agrupaciones Anacrónicas. Hakim Bey me parece excelente para la
intro, si alguien que ya lo tiene bien leido puede poner citas y
contexto que sean útiles, estaría buenisimo.

-Sobre la Libre Cultura

-Sobre la Libertad de Información y Expresión

-Sobre los \"Aprietes\" en Argentina a la Libertad de Información y
Expresión Caso Potel, Canon Digital, convenio CADRA-UBA y otras
universidades, Caso

-Copyrights: Reformar los Copyrights; legalizar formalmente el
intercambio de Cultura. Asegura que cuando una obra creativa sea
comercializada masivamente, el beneficiado sea el Artista y no los
poseedores de los derechos amparados en leyes de Restricción y
anacronismo Cultural.

-Marcas Registradas: El abuso de las marcas registradas, como sustituto
de o añadido a los derechos de autor, es algo que no se puede permitir
que continue, dado que mina la confianza del público. Acorde con ésto,
el material sujeto a derechos de autor no podrá ser susceptible de
convertirse en marca registrada, y del mismo modo las marcas registradas
no podrán quedar sujetas a derechos de autor.

-Patentes: No al patentamiento de la vida animal o vegetal.

-Sobre el Estado Argentino ( Estado Transparente ) Software libre para
la administración pública (obligación de su uso en la administracion
pública).

-Sobre Derechos del Individuo ( defensa de la privacidad, etc. )

-Sobre otros temas fuera de este manifiesto y nuestra política general.
