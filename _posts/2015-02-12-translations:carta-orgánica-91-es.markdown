---
title: Translations:Carta Orgánica/91/es
layout: post
categories: []
---

Las sanciones a los Piratas Afiliados solo serán dispuestas por la
Asamblea pertinente previa denuncia de otro Pirata Afiliado, de descargo
del imputado y de resolución fundada de la Asamblea.
