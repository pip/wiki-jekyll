---
title: Translations:Carta Orgánica/146/es
layout: post
categories: []
---

Mediante el Consenso de una Asamblea General convocada a tal efecto
podrá disolverse el Partido.
