---
title: Plantilla:Propuesta
layout: post
categories:
- Propuestas en curso
- "{{LOCALMONTHNAME}} {{LOCALYEAR}}"
---

<noinclude>

Plantilla para hacer propuestas
===============================

Esta plantilla contiene los campos mínimos para hacer una propuesta y
empezar a trabajarla, creando un pad, categorizando por defecto, etc.

Para usarla, hay que escribir {{Propuesta}} en el cuerpo de un artículo
nuevo.

</noinclude>

<includeonly> [Categoría:Propuestas en
curso](Categoría:Propuestas_en_curso "wikilink")
[Categoría:{{LOCALMONTHNAME}}
{{LOCALYEAR}}](Categoría:{{LOCALMONTHNAME}}_{{LOCALYEAR}} "wikilink")
</includeonly>
