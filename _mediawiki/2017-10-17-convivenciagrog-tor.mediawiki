== Grog & Tor ==
<br />
=== Código de conducta === <br />
==== (aplica para el canal de Telegram/IRC de G&T) ==== <br />
<br />
Con la idea de que este canal sea un espacio libre, inclusivo, seguro, feminista, no sexista, no fascista, no #desistemas(*), pensamos las siguientes reglas:<br />
<br />
* Solo pueden ingresar las personas que hayan asistido a una Grog&Tor <br />
La idea del canal es continuar de manera conjunta el trabajo que hicimos en el taller. Es por esto que preferimos evitar que el grupo se llene de gente que no conocemos.<br />
* Aflojemos con las conversaciones entre chabones cis de sistemas/con conocimiento técnico <br />
Hay miles de canales ya armados para esto. En este canal bienvenimos a todas las personas por igual, tengan conocimientos previos o no, y entendemos que espacios para aprender entre todas son los que faltan. No intimidemos a las demás e invitémoslas a participar! Si usamos términos técnicos, los explicamos para que todas lo entiendan.<br />
* Importante: no toleramos el mansplaining!<br />
Al tener en cuenta a las que tengan menos conocimientos técnicos que una e invitarlas a participar, prestemos atención a no realizar ningún tipo de 
[https://es.wikipedia.org/wiki/Mansplaining mansplaining] hacia las compañeras mujeres cis, trans, travas o lesbianas. No toleraremos esta práctica.
<br />
<br />

Tratamos de aplicar estas reglas a todas las personas que estén en el canal.  Si no se cumplieran, serán invitadas colectivamente a irse P)<br />
<br />

(*)Refiere a los canales o chats formados por gente relacionada con computación y sistemas cuyo fin es compartir conocimientos técnicos. Por lo general están integrados mayoritariamente por varones cis.