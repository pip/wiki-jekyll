Dirección General Electoral:
Av. De Mayo 633 7º piso
4342-4800
www.buenosaires.gob.ar

Tribunal Superior de Justicia:
Cerrito 760
4370- 8500
www.tsjbaires.gob.ar

Juzgado Federal Nº 1 (Secretaría Electoral):
Tucumán 1320 subsuelo
www.pjn.gob.ar

Banco Ciudad (Casa Matriz - Oficina de cuentas corrientes):
Florida 302- P.B.
4329-8886
www.bancociudad.com.ar

AFIP:
www.afip.gob.ar

Tesorería Gobierno de la Ciudad:
Belgrano 840 (de 9:00 a 14:00 hs.)

Auditoria Ciudad De Buenos Aires
Dirección General de Asuntos Institucionales y Partidos Políticos
Av. Corrientes 640 – 5º piso
T.E. 4321-3700 int. 416
Horario de atención de lunes a viernes de 10.00 a 18.00 hs.
Dr. Gustavo Portichella
www.agcba.gov.ar/web/index.php


ORGANISMO RESPONSABLE
Dirección General Electoral
Dirección: Av. de Mayo 633 Piso 7º
Tel.: 4342-4800
E-mail: dgelec@buenosaires.gov.ar
Web: www.buenosaires.gov.ar/areas/seguridad_justicia/dg_electoral/?menu_id=24252


Fecha de última modificación: Julio 2012
Pagina de donde se obtuvieron los datos: http://www.buenosaires.gob.ar/guiaba/guia/index.php?info=detalle&menu=1&id=1595