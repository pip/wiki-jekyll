{| style="margin:0px 0px 0px 0px; width:100%; background:none"
| class="MainPageBG" style="width:100%; border:1px solid #C7D0F8; background:#F2F5FD; vertical-align:top; color:#000; -moz-border-radius:4px" |
{| style="vertical-align:top; background:#F2F5FD; color:#000; width:100%; padding: 2px "
|-
|
<!-------------------------------------------
--- BARRA SUPERIOR (TEXTO IZQUIERDA) --------
-------------------------------------------->
{| style="width:60%; border:solid 0px; background:none"
| style="width:60%; text-align:center; white-space:nowrap; color:#000" |
<div style="font-size:162%; border:none; margin:0; padding:.1em; color:#6E98C2">Bienvenidos a la Wiki del Partido Pirata!!</div>
<div style="font-size:75%; border:none; margin:5; padding:.1em; color:#444">[http://campusvirtual.unex.es/cala/epistemowikia/index.php?title=Comenzar_a_usar_Mediawiki Como usar un MediaWiki]</div>
|}<!-----------------------------------------
--- BARRA SUPERIOR (TEXTO DERECHA) ----------
-------------------------------------------->
| style="width:40%; font-size:100%; color:#000; text-align:right" |
{{CURRENTTIME}} – {{CURRENTDAYNAME}},<br />{{CURRENTDAY}} de {{CURRENTMONTHNAME}} de {{CURRENTYEAR}}.<br />
'''{{NUMBEROFARTICLES}}''' articulos.
|}
|}

__NOTOC__

{| role="presentation" style="border:0; margin: 0;" width="100%" cellspacing="10"
| valign="top" class="mainpage_hubbox" |
<div class="mainpage_hubtitle">
=Partido Pirata=
</div>
<div class="mainpage_boxcontents">

==[[Como Participar]]==
Listas de correo, chat, comunicación y organización.

==Textos fundacionales del Partido Pirata==
===[[Manifiesto_del_Partido_Pirata|Manifiesto]]===
El manifiesto pirata poético votado en asamblea 12/11/2009.

===[[Declaración de Principios Partido Pirata Argentino]]===
Redactada conjuntamente. Basada en el manifiesto poético.

===[[Declaración de Principios Partido Pirata Internacional]]===
http://www2.piratpartiet.se/international/espanol

===Carta Orgánica===
Redactada conjuntamente, en revisión constante.
http://partidopirata.com.ar/documentos/CartaOrganica.html

==[[Preguntas mas frecuentes|Preguntas frecuentes]]==
Para sacarte esas dudas que andan dando vueltas.

==[[Multimedia del Partido]]==
Panfletos, imagenes, videos, logos, emoticons, wallpapers, lo que haya.

==[[PirateFest 2011|PirateFest]]==
Festival anual de filosofía pirata. El primer festival pirata se llevó a cabo en paralelo a la primera Asamblea General del Partido Pirata Internacional. Luego se repitió en 2012.

==[[Artículos de Interés]]==
Esta página contiene artículos de temáticas relacionadas con el proyecto como Cultura Libre, Derechos de Autor, Patentes.


</div>
| valign="top" class="mainpage_hubbox" |
<div class="mainpage_hubtitle">
==Cultura Pirata==
</div>
<div class="mainpage_boxcontents">

===[[Bandas Piratas]]===
Tenemos que mostrar que la alternativa copyleft es posible y para eso hay que difundir
a las bandas piratas.

===[[Libros Piratas]]===
¿Libros, revistas y comics con licencia Creative Commons? También.

===[[Películas Piratas]]===
Películas, documentales y videos copyleft.

===[[Programas Piratas]]===
Software copyleft.

===[[Programas de Radio Pirata]]===
Programas de Radio con Temática Afín.

===[[Maquinaria Pirata]]===
Hardware, herramientas y maquinaria con licencias libres para construir vos mismo.


</div>
| valign="top" class="mainpage_hubbox" |
<div class="mainpage_hubtitle">
==[[Proyectos|Proyectos y campañas]]==
</div>
<div class="mainpage_boxcontents">
Ideas, proyectos, campañas, etcéteras.

===[[Radio del Partido Pirata]]===
Nos gustaría crear nuestro podcast, si desea participar de él, envíe un e-mail a: '''ppirataargentino#gmail.com'''  (reemplace # por @).

===[[Proyectos sociales colaborativos]]===
La idea es crear proyectos sociales colaborativos que tomen propia vida, para atacar problemas sociales (como la crisis habilitacional, inserción laboral, etc) desde las bases.

===[[Observatorio Penal]]===
Causas judiciales, aprietes legales, proyectos de ley, notas de interés y mas.

===[[Nic-ar Censura?]]===
Listado de dominios .com.ar que la entidad administradora en Argentina bloquea.

===[[Observatorio de ISPs]]===
¿Hay proveedores filtrando nuestro trafico? ¿Dándonos menos de lo que nos venden? Este es el lugar para investigar, comparar y denunciar estas practicas.

===[[Observatorio Telefonicas]]===
¿Tenes problemas con proveedores de acceso telefonico o fabricantes de telefonos? Compartilos.

===[[Traducciones]]===
Traducciones de artículos interesantes que distintos piratas fueron llevando a cabo.


</div>
|}



{| role="presentation" style="border:0; margin: 0;" width="100%" cellspacing="10"
| valign="top" class="mainpage_hubbox" |
<div class="mainpage_hubtitle">
==Guias y tutoriales==
</div>
<div class="mainpage_boxcontents">

===[[Guia P2P]]===
Guia colaborativa sobre como funciona internet, como funciona la censura en internet y como esquivarla.

===[[Torrents/Magnets]]===
No se puede prohibir un standard, o se puede? Qué son y qué formas creativas podemos encontrar para usarlos?

===[[Subtítulos]]===
Como hacerlos, de donde bajarlos y como combinarlos con los videos.

===[[Mumble]]===
Charlá y chateá por internet sin depender de un servidor central ni software privativo.

===[[Chili - Administrador de proyectos]]===
Para la organización interna y la división de tareas utilizamos un administrados de proyectos libre llamado Chilli.

===[[Operación Leviatán Electrónico]]===
Privacidad, cifrado, comunicación anónima y compartir archivos de forma segura.

===[[Crear proxy The Pirate Bay]]===
Cómo crear tu propio proxy a The Pirate Bay.

===[http://virtual.flacso.org.ar/pluginfile.php/389/mod_page/content/16/Guia_de_licencias_Creative_Commons_v2.pdf Guía para registrar una obra con Creative Commons]===
¿Qué son las licencias Creative Commons?, ¿Cómo registrar una obra con Creative Commons?

===OTR===
Prueba [[Pidgin con OTR]] o [[Gibberbot con OTR]] para Android.

</div>
| valign="top" class="mainpage_hubbox" |
<div class="mainpage_hubtitle">

==Información==
</div>
<div class="mainpage_boxcontents">

===[[Cannabis]]===
Información sobre los diferentes usos del cannabis, estado legal, agrupaciones, etc.

===[[Derecho de Autor]]===
En esta página podrás encontrar las leyes vigentes relacionadas con el derecho de autor y proyectos presentados en organismos legislativos.

</div>
| valign="top" class="mainpage_hubbox" |
<div class="mainpage_hubtitle">
==Ideas==
</div>
<div class="mainpage_boxcontents">

===[[Compartir es bueno]]===
Campaña enfocada a instalar la idea de que compartir es bueno y no tiene por que ser ilegal.

===[[Política Digital]]===
¿Qué es una políica digital? ¿a qué está enfocada?

===[[Adopta un político]]===
Programa enfocado a establecer un dialogo con diputados, senadores y demás actores políticos. Muchos de los cuales desconocen los objetivos del movimiento pirata.

===[[A quien votar]]===
Cuestionario para hacerle a candidatos a senadores y diputados, para saber a quien votar.

===[[Cooperativas sociales y estatales]]===
¿Que seria una cooperativa social y una cooperativa estatal? ¿Que beneficios traerian? ¿Como se podrian promover y priorizar desde el estado? 

</div>
|}