La idea es publicar borradores y modificarlos libremente (mientras se van discutiendo) para despues, dada una fecha, votar por el que le parezca a la mayoria cual es el que representa mejor al grupo.


== Manifiestos de partidos de otros paises. ==

[[Manifiesto Pirata Borrador (España)]]

[[Declaración de Principios 3.2 (Suecia)]]

[[Manifiesto WikiPartido Pirata Mexicano]]

'''En Portugués/Español:'''

[[Perguntas e Respostas sobre o Partido Pirata (Brasil)]]


== Las bases del Manifiesto ==


Suecia, España, Brasil, etc, básicamente todos tienen
algunas ideas en común, por ejemplo:



Patentes

Derechos de autor

Copyright

Propiedad Intelectual

Universalización de Internet/Inclusión Digital

Transparencia de los gobiernos y su control vía Internet

Monopolios Privados (oposición a los monopolios privados)

Uso de software libre en la administración pública

__________________________

Sobre las patentes las dividen en 3 grandes grupos:

Patentes de software.

Patenes de productos farmacéuticos

Patentes sobre la vida, genes/plants.

________________________

Los españoles colocan el tema del Canon Digital (que siempre está
amenazando en la Argentina) en la parte de propiedad intelectual.

Los suecos hablan de "Liberar nuestra cultura"

_______________________


== Puntos conflictivos derivados de la primera reunión. ==

''Dejo los puntos conflictivos que quedaron de la primera reunión. Agreguen, borren, y reescriban lo que deseen.'' 


Delimitar el significado de lo que llamamos libre cultura (discusión Shakira No/ Heidegger Si). Definir que si y que no? O debemos abarcar todo?.

Producción, comercialización y venta ambulante de cultura. Los grandes productores de dvds y cds "truchos". El vendedor ambulante... util a la causa?. La Salada y similares. 

Partido Monotemático o Apertura hacia los diferentes problemas sociales.

Nos basamos en los Problemas de derechos a la cultura en Internet, sumamos los problemas de derechos a la cultura de la sociedad de masas?

El partido es la cara visible, pulcra y políticamente correcta? 

Correlatividad con las ideas del Partido Pirata Internacional o simple aprovechamiento del nombre? Otro partido de Izquierda?