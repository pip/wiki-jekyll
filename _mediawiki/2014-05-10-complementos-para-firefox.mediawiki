[[Category:Privacidad]] [[Category:Software_Libre]]

= ¿Como instalar complementos? =

Podes instalar complementos desde la página de este en Mozilla ([https://addons.mozilla.org/en-US/firefox/addon/ghostery/ Ejemplo]) dandole al boton verde. 

O desde el mismo navegador yendo a ''Herramientas -> Complementos -> Buscar complementos'' y buscando el nombre del complemento que queremos instalar. 

Una vez instalados deberian aparecer en ''Complementos -> Extensiones'' y desde ahi mismo se pueden desinstalar.

= No instrusivos =

Luego de instalar estos complementos '''todo deberia funcionar normalmente'''.

* [https://www.eff.org/https-everywhere/ HTTPSEverywhere] Conectate de forma segura a los sitios web.

* [[Perspectives_(Navegación_Segura)]] es una red de servidores que actúan como "testigos" de los certificados seguros de los sitios web. 

* [https://adblockplus.org AdBlock Plus] bloquea las molestas publicidades que te rastrean en Internet.

* [https://www.ghostery.com Ghostery] bloquea el analisis de comportamiento que te rastrea en Internet.

= Intrusivos =

Luego de instalar estos complementos '''algunos sitios podrían no funcionar correctamente''' hasta que hayas configurado el complemento.

* [https://addons.mozilla.org/en-US/firefox/addon/noscript/ NoScript] Bloquea todo el JavaScript y los objetos Flash, salvo aquellos que vos permitas. Sirve para evitar el rastreo y virus cuando visitas sitios desconocidos.