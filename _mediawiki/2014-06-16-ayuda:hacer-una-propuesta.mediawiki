Las [[:Categoría:Propuestas|propuestas]] son la forma en que presentamos ideas y acciones en las asambleas piratas.  Se espera que la persona que hace la propuesta se haga responsable de llevarla a cabo.

Todas las propuestas siguen el proceso de consenso desde que se presentan hasta que se realizan!

== Para hacer una propuesta usando el wiki ==

* Ir a [[Plantilla:Propuesta]] y copiar la plantilla
* Crear un artículo nuevo y pegar la plantilla
* Modificar a gusto
* Enviar al [[Barcos Piratas|Barco Pirata]] que corresponda!

[[Categoría:Barco_de_infraestructura|{{PAGENAME}}]]
[[Categoría:Organización|{{PAGENAME}}]]
[[Categoría:Propuestas|{{PAGENAME}}]]