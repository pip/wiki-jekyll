Instructivo
 
¿En qué consiste, para qué y cuándo realizarlo?
El trámite consiste en la tramitación de los aportes públicos regulados por la Ley  Ley Nº 268 y la Resolución Nº 301/MJYSGC/2009 a disposición de los Partidos Políticos que participen en las elecciones a Legisladores de la Ciudad a realizarse el próximo 28 de junio de 2009. 
 	 
Información por internet 
Enlace	Tipo de enlace 
Resolución Nº 301/MJYSGC/2009	 Más información 

 
¿Dónde y en qué horario se realiza?
Apertura de Cuenta de Partidos Políticos: Casa Matriz del Banco Ciudad (Florida 302- P.B) 

Dirección General Electoral: Av. De Mayo 633, 7º piso. 


 
¿Quién puede efectuarlo?
Los representantes de los Partidos  Políticos que presenten candidaturas en las próximas elecciones. 


 
¿Qué se necesita para realizarlo?
Pasos a seguir para la tramitación del aporte público

Paso 1:

Apertura de cuenta en el Banco Ciudad de Buenos Aires

a) Atento a lo dispuesto en el artículo 1 inciso b) de la Resolución Nº 301/MJYSGC/2009, los partidos políticos deberán abrir una cuenta en la Casa Matriz del Banco Ciudad Florida 302 P.B. a nombre del partido y a la orden conjunta de hasta cuatro miembros de la agrupación política. A los efectos de dar cumplimiento a lo establecido en el inciso d) del mencionado artículo, previo a todo trámite, los miembros del partido, a la orden de los cuales se realice la apertura de la cuenta, deberán ser designados por el órgano partidario que corresponda, y el acta de designación se presentará ante el Tribunal Superior de Justicia (TSJ) para su certificación.

b) Requisitos para la apertura de la Cuenta de Partidos Políticos.

• Carta Orgánica del partido político.
• Resolución de Reconocimiento de Personería Jurídica ante el Juez con competencia
electoral correspondiente.
• Constancia de domicilio: servicio público o impuesto a nombre del partido o certificación
emanada del Juez con competencia electoral.
• Actas de designación de autoridades del partido.
• Certificación expedida por el TSJ de la designación de las personas autorizadas para
operar la cuenta de campaña.
• Comprobante de Inscripción en la AFIP (CUIT – Clave Única de Identificación Tributaria).

Los partidos que posean ya su número de CUIT podrán obtener este certificado a través de Internet en la Web oficial de la AFIP. Quienes aún no lo hayan tramitado deberán realizarlo sin excepción antes de iniciar la apertura de la cuenta, en la agencia que corresponda al domicilio del partido.

• Documento de Identidad de los firmantes: L.E; L.C o D.N.I o Cédula de Identidad.
• CUIT/CUIL de los firmantes.

Importante:

• Toda la documentación que solicite el Banco Ciudad debe estar certificada por escribano o adjuntar el original para su cotejo. Las firmas serán registradas por personal del Banco o deberán ser certificadas por escribano.

Paso 2:

Presentación de la documentación en la Dirección General Electoral.
Los representantes de los partidos que presenten candidaturas en las próximas elecciones deberán remitir la documentación a la Dirección General Electoral de lunes a viernes de 11:00 a 18:00 hs.

1- Nota de solicitud de aporte público firmada por apoderado del partido. En el caso de las alianzas que soliciten que el aporte público de los partidos que la integran sean depositados en la misma cuenta, deberán hacer manifiesta esta voluntad y la solicitud deberá estar firmada por todos los apoderados de los partidos que la componen adjuntando el acta certificada del organismo competente partidario que disponga dicha solicitud. En ambos casos se deberá adjuntar certificación expedida por el Juzgado Federal Nº 1 o el TSJ de la condición de apoderado de cada agrupación.

2- Resolución de Oficialización de lista de candidatos expedida por el TSJ.

3- En el caso de partidos que hubieran participado en la elección de legisladores/as del año 2007 integrando alianzas, deben presentar el acuerdo de distribución de fondos formalizado en esa oportunidad.

4- Formulario de “Autorización de acreditación de pagos del Tesoro del Gobierno de la Ciudad de Buenos Aires en cuenta bancaria”: el documento deberá ser previamente completado y firmado por la sucursal del banco en la cual se realiza la apertura de la cuenta.

5- Certificación expedida por el TSJ de la designación de las personas autorizadas para operar la cuenta de campaña.

6- Constancia del domicilio constituido ante el TSJ expedido por el mismo.

7- Constancia de inscripción ante la AFIP.

Paso 3:

Presentación de Informe ante la Auditoría General de la Ciudad de Buenos Aires.
En cumplimiento del artículo 17 de la Ley 268, los partidos políticos o alianzas deberán presentar un informe indicando ingresos y egresos efectuados con motivo de la campaña electoral y el presupuesto de los ingresos y egresos que se prevén efectuar hasta la finalización de la campaña. La presentación se efectúa ante la Auditoría General de la Ciudad.
 	 
Información por internet 
Enlace	Tipo de enlace 
Formulario de Autorización Acreditación	 http://estatico.buenosaires.gov.ar/areas/seguridad_justicia/dg_electoral/pdf/autorizacion_acreditacion_tesoro.pdf?menu_id=30375

Datos obtenidos de: http://www.buenosaires.gob.ar/guiaba/guia/index.php