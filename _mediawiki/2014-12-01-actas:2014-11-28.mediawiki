= Asamblea general pirata (noviembre 2014) =

== Participantes ==

* diego
* max power
* coursociety
* mpj
* seykron
* aza
* minitrue
* antares
* facundo
* nicoff
* tom
* fauno

Lugar: Bar cooperativo La Barbarie

Horario de inicio: 18:30

Horario de finalización: 21:30

== Por qué el lugar ==

La gente copada, y como es bar cooperativo autogestivo necesitan que vaya gente para sostenerlo.

== Resumen de actividades de accion directa del ultimo año ==

=== Yo los paro (contó minitrue) ===

* Primer acercamiento a Proyecto Sur: [http://wiki.partidopirata.com.ar/Propuesta:Reuni%C3%B3nProyectoSur quieren cambiar la forma de participación en GESTA].
* Surge la ley de hidrocarburos: nos dan una lista de diputados en una reunión y de ahí sale [http://wiki.partidopirata.com.ar/YoLosParo YoLosParo].
* Repercusiones: 22K de mensajes. Una diputada reenvía los emails a los que le mandan mensajes. Recital de las Pastillas y las Perras que los Parió, video de Cordera; respuesta de Abraham en la cámara; diputados cerraron sus cuentas en redes sociales.
* Aprendizaje: cómo funciona la política día a día. Reunión de la multisectorial de la CTA (FUA, MST, CCC, Proyecto Sur, Asambleas por el Agua, No a Monsanto). La CTA coordina y pone la guita para los equipos de audio, así que la CTA decide cómo se organiza la reunión.

==== Acciones ====

* Mostrar los resultados en el sitio
* Enviar email con los resultados y difundir por las redes sociales.
* Buscar más participación de las piratas.

=== Amparo Pirata (contó aza) ===

* CAPIF a través de una medida autosatisfactiva logra bloquear a TPB (dominios e IPs). Abogado amigo nos ayudó y [http://wiki.partidopirata.com.ar/BloqueoThePirateBay presentamos un amparo] por el contenido de un pirata cordobés.
* El primer juez se declaró incompetente, y se asigna el juzgado por sorteo. El juez mandó a pedir el expediente de CAPIF.
* La medida autosatisfactiva debería caer a los 90 días, pero no se hace seguimiento y la CNC aún mantiene el bloqueo.
* Resultados positivos: aprendimos y documentamos muchísimo sobre los procesos legales involucrados.
* Amenazas: fue difícil conseguir gente para participar, activar, falta participación incluso. Facundo dice que &quot;nunca se siente&quot; que necesitemos ayuda. Hay que mejorar la comunicación, pedir ayuda sobre tareas específicas.

==== Acciones ====

* Tratar de hacer caer la medida autosatisfactiva.
* Revivir la campaña [http://wiki.partidopirata.com.ar/ShareLikeAPirate Share like a pirate]
* Max power cede la cuenta de pirate bay para subir torrents.
* El tema del hashtag en inglés es por la difusión internacional, de última podemos usar los dos.

=== Argentina Digital (contó seykron) ===

* Nos contactan para que hagamos &quot;consultoría&quot; sobre el proyecto de ley [http://wiki.partidopirata.com.ar/Propuesta:LeyArgentinaDigital Argentina Digital].
* En general nos parece bien, pero pensamos que podemos meter fomento a las redes comunitarias, asi que contactamos con Altermundi (e.g. QuintanaLibre, DeltaLibre).

== Realpolitik (Política garca) ==

* Viendo las cosas que aprendimos de estas acciones directas sobre la forma de transar en esta democracia representativa, nos sigue pareciendo estratégico formalizar el partido?

Seykron dice: &quot;no hay chances de influir en la decisión política. Todo lo que es trabajo en comisión es humo para la gilada&quot;.

* Algunas personas (asesores, legisladores) sí parecían interesadas en los temas tratados, pero la sensación de las que participaron es que la decisión bajaba de arriba y no era relevante el debate abajo. De todas maneras este (el trabajo en comisión) parece ser el mejor momento para meter cambios superadores no muy radicales.

== Definir las fotalezas y debilidades del partido pirata ==

¿Cuál es el alcance de las estrategias de acción directa?, ¿cómo articularse con las formas institucionales de participación política?

Hay consenso sobre la estrategia política: va a ir por fuera de lo institucional, de la realpolitik. Si hay que constituir una religión, es una estrategia válida.

== Objetivos para el periodo 2015 ==

* Empezar a ver las maneras de encarar [http://wiki.partidopirata.com.ar/Documentos_Fundacionales los procesos] más que los objetivos concretos. Prepararse para la próxima situación que surja (organizar los barcos).
* Facilitar el ingreso de las personas y promover la participación. Descubrir las barreras y romperlas.
* Pensar la accesibilidad de las herramientas.
* Organizar la integración entre piratas y barcos
* Declarar al Partido Pirata como espacio seguro: no juzgar, no criticar errores.
* Objetivo externo provisorio: difundir cultura y prácticas piratas, fortalecer bases amicales.