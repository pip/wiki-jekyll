{{Propuesta
| descripcion = Sistematizar la creacion de nuevas propuestas
| estado = nuevas
| asamblea = https://asambleas.partidopirata.com.ar/archivos/general-partidopirata.com.ar/
| responsable = minitrue
}}


== Problema ==

No esta sistematizada la creación de propuestas y no hay una guía sencilla para que piratas nuevos o piratas que aun no tienen herramientas técnicas moderadas(uso de la mediawiki) puedan crear propuestas.

== Idea ==

=== Sistematizacion de las propuestas ===
* Definir los pasos a seguir y caracteristicas que debe tener una propuesta
* Si es posible, definir distintas categorias de propuestas con requisitos minimos cada una.


=== Guia para la creacion de propuestas y paginas en la wiki ===
* Armar un tutorial que le de las herramientas necesarias a los novatos para crear propuestas y paginas en la wiki.
* Proponer un primer ejercicio donde se  modifique la pagina principal de cada pirata "https://wiki.partidopirata.com.ar/Pirata:Croatan"

[[Categoría:Minitrue]]
[[Categoría:Propuestas]]
[[Categoría:Propuestas_nuevas]]
[[Categoría:Propuestas_por_minitrue]]

[[Categoría:Minitrue]]
[[Categoría:Propuestas]]
[[Categoría:Propuestas_nuevas]]
[[Categoría:Propuestas_por_minitrue]]