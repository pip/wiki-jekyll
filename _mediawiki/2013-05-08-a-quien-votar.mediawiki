La idea es armar un cuestionario para candidatos que nos diga su opinión sobre ciertos temas puntuales y proyectos relevantes al tema para ver como ha votado en el pasado, sabiendo así si dice la verdad o no.

==Formato==
Ejemplo del formato.

===¿Cual es su posición con respecto a la minería a cielo abierto?===

'''Respuestas posibles:''' 
* Debe ser permitida.
* Debe ser permitida con controles, limitaciones y alejada de fuentes de agua.
* Dese ser prohibida.

'''Proyectos de ley relevantes:''' 
* Ley de protección de glaciares

==Preguntas==